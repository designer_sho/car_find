package com.felizcube.bleremote.DataRestore;


import android.content.Context;
import android.content.SharedPreferences;

import com.felizcube.bleremote.Tool;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.felizcube.bleremote.DataRestore.CarData.Car.KEY_NAME;
import static com.felizcube.bleremote.DataRestore.CarData.Car.KEY_PHOTO;
import static com.felizcube.bleremote.DataRestore.CarData.Car.KEY_SELECT;

public class CarData
{
  private static final String FILE = "DATA";
  private static final String KEY_CAR_DATA = "CAR_DATA";
  private static final String KEY_PROFILE = "PROFILE";
  private static SharedPreferences settings;
  public static ArrayList<Car> cars = new ArrayList<>();

  public CarData(Context ctx)
  {
    settings = ctx.getSharedPreferences(FILE,0);
    LoadData(settings.getString(KEY_CAR_DATA,""));
  }

  private void LoadData(String str)
  {
    if(str.length() == 0)
    {
      cars.clear();
      return;
    }
    else
    {
      cars.clear();
      try
      {
        JSONObject json = new JSONObject(str);
        if(json.has(KEY_PROFILE))
        {
          JSONArray temp = json.getJSONArray(KEY_PROFILE);
          for(int i = 0 ; i < temp.length() ; i++)
          {
            cars.add(new Car(temp.get(i).toString()));
          }
        }
      }
      catch(Exception e)
      {
        Tool.LOG(e.getMessage());
      }
    }
  }

  public static void SaveData()
  {
    JSONObject obj = new JSONObject();
    try
    {
      JSONArray temp = new JSONArray();
      for(int i = 0 ; i < cars.size() ; i++)
      {
        JSONObject car = new JSONObject();
        car.put(KEY_NAME,cars.get(i).name);
        car.put(KEY_PHOTO,cars.get(i).photo);
        car.put(KEY_SELECT,cars.get(i).select);
        temp.put(car);
      }
      obj.put(KEY_PROFILE,temp);
    }
    catch(Exception e)
    {
      Tool.LOG(e.getMessage());
    }
    Tool.LOG(obj.toString());
    settings.edit().putString(KEY_CAR_DATA,obj.toString()).commit();
  }

  public void AddPhoto(int index,String photo)
  {
    cars.get(index).photo = photo;
    SaveData();
  }

  public void AddNewCar(String name,String photo)
  {
    Car car = new Car(name,photo);
    car.select = 1;
    cars.add(car);
    SaveData();
  }

  public void DeleteCar(int index)
  {
    cars.remove(index);
    SaveData();
  }

  public void SelectCar(int index)
  {
    for(int i = 0 ; i < cars.size() ; i++)
    {
      cars.get(i).select = 0;
    }
    cars.get(index).select = 1;
    SaveData();
  }

  public int GetSelectCarIndex()
  {
    int result = - 1;
    for(int i = 0 ; i < cars.size() ; i++)
    {
      if(cars.get(i).select == 1)
      {
        result = i;
      }
    }
    return result;
  }

  public void RemoveSelectCar()
  {
    for(int i = 0 ; i < cars.size() ; i++)
    {
      cars.get(i).select = 0;
    }
  }

  public void ChangeCarName(String name)
  {
    cars.get(GetSelectCarIndex()).name = name;
  }

  public boolean IsSameName(String name)
  {
    boolean result = false;
    for(int i = 0 ; i < cars.size() ; i++)
    {
      if(name.equals(cars.get(i).name))
      {
        result = true;
      }
    }
    return result;
  }

  public class Car
  {
    public String name;
    public String photo;
    public int select;
    public static final String KEY_NAME = "NAME";
    public static final String KEY_PHOTO = "PHOTO";
    public static final String KEY_SELECT = "SELECT";

    public Car(String name,String photo)
    {
      this.name = name;
      this.photo = photo;
      this.select = 0;
    }

    public Car(String str)
    {
      try
      {
        JSONObject json = new JSONObject(str);
        this.name = json.get(KEY_NAME).toString();
        this.photo = json.get(KEY_PHOTO).toString();
        this.select = json.getInt(KEY_SELECT);
      }
      catch(Exception e)
      {
        Tool.LOG(e.getMessage());
      }
    }
  }

}
