package com.felizcube.bleremote.DataRestore;

import android.content.Context;
import android.content.SharedPreferences;

import com.felizcube.bleremote.Tool;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class UIData
{
  private static final String FILE = "DATA";
  private static final String KEY_UIDATA = "UI_DATA";
  private static final String KEY_PRIORITY = "PRIORITY";
  private static final String KEY_CODE = "CODE";
  private static final String KEY_CODE_ENABLE = "CODE_ENABLE";
  private static SharedPreferences settings ;
  public static String code = "";
  public static boolean enable_password = false;
  public static ArrayList priority = new ArrayList();
  public UIData(Context ctx)
  {
    settings = ctx.getSharedPreferences(FILE, 0);
    LoadData(settings.getString(KEY_UIDATA,""));
  }

  private void LoadData(String str)
  {
    if(str.length() == 0)
    {
      CreateUIData();
    }
    else
    {
      try
      {
        JSONObject json = new JSONObject(str);
        if(json.has(KEY_PRIORITY))
        {
          JSONArray temp = json.getJSONArray(KEY_PRIORITY);
          if(temp.length() != 6)
          {
            CreatePriority();
          }
          else
          {
            for(int i = 0 ; i < temp.length() ; i++)
            {
              priority.add(temp.get(i).toString());
            }
          }
        }
        else
        {
          CreatePriority();
        }
        if(json.has(KEY_CODE))
        {
          code = new String(json.getString(KEY_CODE));
          enable_password = json.getBoolean(KEY_CODE_ENABLE);
        }
        else
        {
          CreateCode();
        }
      }
      catch(Exception e)
      {
        Tool.LOG(e.getMessage());
        CreateUIData();
      }
    }
  }

  private void CreateUIData()
  {
    CreatePriority();
    CreateCode();
    Tool.LOG("Create New Data");
  }

  private void CreatePriority()
  {
    priority.add("unlock");
    priority.add("start");
    priority.add("lock");
    priority.add("aux1");
    priority.add("panic");
    priority.add("trunk");
  }

  private void CreateCode()
  {
    code = "";
    enable_password = false;
  }

  public static void SaveData()
  {
    JSONObject obj = new JSONObject();
    try
    {
      JSONArray temp = new JSONArray();
      for(int i =0 ;i<priority.size();i++)
      {
        temp.put(priority.get(i).toString());
      }
      obj.put(KEY_PRIORITY,temp);
      obj.put(KEY_CODE,code);
      obj.put(KEY_CODE_ENABLE,enable_password);
    }
    catch(Exception e)
    {
      Tool.LOG(e.getMessage());
    }
    settings.edit().putString(KEY_UIDATA, obj.toString()).commit();
  }

}
