package com.felizcube.bleremote.DataRestore;

import android.content.Context;
import android.content.SharedPreferences;

import com.felizcube.bleremote.Tool;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class SVCData
{
  private static final String FILE = "DATA";
  private static final String KEY_SVC_DATA = "SVC_DATA";
  private static final String KEY_SVC = "SVC";
  private static SharedPreferences settings;
  public static ArrayList<Integer> svcs = new ArrayList<>();

  public SVCData(Context ctx)
  {
    settings = ctx.getSharedPreferences(FILE,0);
    LoadData(settings.getString(KEY_SVC_DATA,""));
  }

  private void LoadData(String str)
  {
    if(str.length() == 0)
    {
      svcs.clear();
      return;
    }
    else
    {
      svcs.clear();
      try
      {
        JSONObject json = new JSONObject(str);
        if(json.has(KEY_SVC))
        {
          JSONArray temp = json.getJSONArray(KEY_SVC);
          for(int i = 0 ; i < temp.length() ; i++)
          {
            svcs.add(Integer.parseInt(temp.get(i).toString()));
          }
        }
      }
      catch(Exception e)
      {
        Tool.LOG(e.getMessage());
      }
    }
  }

  public static void SaveData()
  {
    JSONObject obj = new JSONObject();
    try
    {
      JSONArray temp = new JSONArray();
      for(int i = 0 ; i < svcs.size() ; i++)
      {
        temp.put(Integer.parseInt(svcs.get(i).toString()));
      }
      obj.put(KEY_SVC,temp);
    }
    catch(Exception e)
    {
      Tool.LOG(e.getMessage());
    }
    Tool.LOG(obj.toString());
    settings.edit().putString(KEY_SVC_DATA,obj.toString()).commit();
  }

  public class SVC
  {
    public String name;
    public int progress;
    public static final String KEY_NAME = "NAME";
    public static final String KEY_PROGRESS = "PROGRESS";

    public SVC(String name,int progress)
    {
      this.name = name;
      this.progress = progress;
    }

    public SVC(String str)
    {
      try
      {
        JSONObject json = new JSONObject(str);
        this.name = json.get(KEY_NAME).toString();
        this.progress = json.getInt(KEY_PROGRESS);
      }
      catch(Exception e)
      {
        Tool.LOG(e.getMessage());
      }
    }
  }
}
