package com.felizcube.bleremote;

import android.graphics.Canvas;
import android.graphics.Point;
import android.view.View;

public class DragShadowView extends View.DragShadowBuilder
{
  private View shadow;
  private Point mScaleFactor;
  public DragShadowView(View v)
  {
    super(v);
    shadow = v;
  }

  @Override public void onProvideShadowMetrics(Point size,Point touch)
  {
    int width;
    int height;
    width = getView().getWidth() * 2;
    height = getView().getHeight() * 2;
    size.set(width, height);
    mScaleFactor = size;
    touch.set(width / 2, height / 2);
  }

  @Override public void onDrawShadow(Canvas canvas)
  {
    canvas.scale(mScaleFactor.x/(float)getView().getWidth(), mScaleFactor.y/(float)getView().getHeight());
    shadow.draw(canvas);
  }
}