package com.felizcube.bleremote;

/**
 * Created by LutherHsieh on 2017/7/11.
 */

public class BleCmdObj
{

  public static final int CMD_NONE = 0;
//  public static final int CMD_VEHICLE_AUTH = 1;
  public static final int CMD_VEHICLE_CONNECT = 2;
  public static final int CMD_VEHICLE_TEST = 3;
  public static final int CMD_VEHICLE_RECONNECT = 4;
  public static final int CMD_VEHICLE_DISCONNECT = 5;
  public static final int CMD_VEHICLE_SET_TIME = 6;
  public static final int CMD_VEHICLE_GHOST = 7;

  public int cmd;
  public VehicleObj vehicleObj;
  public long tick;
  public long workTick;
  public int timeout;
  public int retryCount;

  public static String getCommandName(int cmd)
  {
    String res = "Unknown";
    switch(cmd)
    {
//      case CMD_BATTERY_AUTH:
//        res = "Cmd_Authorization";
//        break;
      case CMD_VEHICLE_CONNECT:
        res = "Cmd_Connect";
        break;
      case CMD_VEHICLE_DISCONNECT:
        res = "Cmd_Disconnect";
        break;
      case CMD_VEHICLE_RECONNECT:
        res = "Cmd_Reconnect";
        break;
      case CMD_VEHICLE_TEST:
        res = "Cmd_Vehicle_Test";
        break;
      case CMD_VEHICLE_SET_TIME:
        res = "Cmd_Vehicle_Set_Time";
        break;
      case CMD_VEHICLE_GHOST:
        res = "Cmd_Vehicle_Ghost";
        break;
    }
    return res;
  }


  public void copyFrom(BleCmdObj bleCmd)
  {
    this.cmd = bleCmd.cmd;
    this.vehicleObj = bleCmd.vehicleObj;

    this.tick = bleCmd.tick;
    this.workTick = bleCmd.workTick;
    this.timeout = bleCmd.timeout;
    this.retryCount = bleCmd.retryCount;
  }

}
