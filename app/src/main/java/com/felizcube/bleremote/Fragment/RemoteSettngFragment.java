package com.felizcube.bleremote.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import com.felizcube.bleremote.R;
import com.felizcube.bleremote.Tool;


public class RemoteSettngFragment extends DialogFragment
{
  private ImageView back;
  private NumberPicker time_start_hour;
  private NumberPicker time_start_minute;
  private NumberPicker time_start_second;
  private NumberPicker run_time_hour;
  private NumberPicker run_time_minute;
  private NumberPicker run_time_second;
  private NumberPicker temperature;
  private LinearLayout save;


  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    final LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    final Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.page_remote_setting,container,false);
    InitUI(view);
    InitUIFunction();
    return view;
  }

  private void InitUI(View view)
  {
    back = (ImageView)view.findViewById(R.id.remote_back);
    time_start_hour = (NumberPicker)view.findViewById(R.id.remote_time_start_hour);
    time_start_minute = (NumberPicker)view.findViewById(R.id.remote_time_start_minute);
    time_start_second = (NumberPicker)view.findViewById(R.id.remote_time_start_second);
    run_time_hour = (NumberPicker)view.findViewById(R.id.remote_run_time_hour);
    run_time_minute = (NumberPicker)view.findViewById(R.id.remote_run_time_minute);
    run_time_second = (NumberPicker)view.findViewById(R.id.remote_run_time_second);
    temperature = (NumberPicker)view.findViewById(R.id.remote_temp);
    save = (LinearLayout)view.findViewById(R.id.remote_save);
    time_start_hour.setMinValue(0);
    time_start_hour.setMaxValue(12);
    time_start_hour.setValue(6);
    time_start_hour.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    time_start_hour.setWrapSelectorWheel(false);
    time_start_minute.setMinValue(0);
    time_start_minute.setMaxValue(60);
    time_start_minute.setValue(30);
    time_start_minute.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    time_start_hour.setWrapSelectorWheel(false);
    time_start_second.setMinValue(0);
    time_start_second.setMaxValue(60);
    time_start_second.setValue(30);
    time_start_second.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    time_start_second.setWrapSelectorWheel(false);
    run_time_hour.setMinValue(0);
    run_time_hour.setMaxValue(12);
    run_time_hour.setValue(6);
    run_time_hour.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    run_time_hour.setWrapSelectorWheel(false);
    run_time_minute.setMinValue(0);
    run_time_minute.setMaxValue(60);
    run_time_minute.setValue(30);
    run_time_minute.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    run_time_minute.setWrapSelectorWheel(false);
    run_time_second.setMinValue(0);
    run_time_second.setMaxValue(60);
    run_time_second.setValue(30);
    run_time_second.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    run_time_second.setWrapSelectorWheel(false);
    String[] values = new String[82];
    for(int i = 0 ; i < values.length ; i++)
    {
      values[i] = (i - 40 + " °F");
    }
    temperature.setMaxValue(values.length - 1);
    temperature.setMinValue(0);
    temperature.setDisplayedValues(values);
    temperature.setValue(40);
    temperature.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    temperature.setWrapSelectorWheel(false);
  }

  private void InitUIFunction()
  {
    back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        RemoteSettngFragment.this.dismiss();
      }
    });
    save.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        Tool.LOG("" + time_start_hour.getValue());
        Tool.LOG("" + time_start_minute.getValue());
        Tool.LOG("" + time_start_second.getValue());
        Tool.LOG("" + run_time_hour.getValue());
        Tool.LOG("" + run_time_minute.getValue());
        Tool.LOG("" + run_time_second.getValue());
        Tool.LOG("" + (temperature.getValue() - 40));
        RemoteSettngFragment.this.dismiss();
      }
    });
  }

}
