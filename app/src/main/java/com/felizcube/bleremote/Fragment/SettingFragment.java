package com.felizcube.bleremote.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.felizcube.bleremote.R;

public class SettingFragment extends DialogFragment
{
  private ImageView back;
  private ImageView app_theme;
  private ImageView cars;
  private ImageView hands_free_setting;
  private ImageView event_history;
  private ImageView schedule_remote_start;
  private ImageView feature_configuration;
  private ImageView sensor_adjustment;
  private ImageView add_new_phone;
  private ImageView paired_device;

  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    final LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    final Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.page_setting,container,false);
    InitUI(view);
    InitUIFunction();
    return view;
  }

  private void InitUI(View view)
  {
    back = (ImageView)view.findViewById(R.id.setting_back);
    app_theme = (ImageView)view.findViewById(R.id.setting_app_theme);
    cars = (ImageView)view.findViewById(R.id.setting_cars);
    hands_free_setting = (ImageView)view.findViewById(R.id.setting_hands_free_setting);
    event_history = (ImageView)view.findViewById(R.id.setting_event_history);
    schedule_remote_start = (ImageView)view.findViewById(R.id.setting_schedule_remote_start);
    feature_configuration = (ImageView)view.findViewById(R.id.setting_feature_configuration);
    sensor_adjustment = (ImageView)view.findViewById(R.id.setting_sensor_adjustment);
    add_new_phone = (ImageView)view.findViewById(R.id.setting_add_new_phone);
    paired_device = (ImageView)view.findViewById(R.id.setting_paired);
  }

  private void InitUIFunction()
  {
    back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        SettingFragment.this.dismiss();
      }
    });
//    app_theme.setOnClickListener(new View.OnClickListener()
//    {
//      @Override public void onClick(View v)
//      {
//        RemoteSettngFragment fragment = new RemoteSettngFragment();
//        fragment.show(getFragmentManager(),"");
//      }
//    });
//    cars.setOnClickListener(new View.OnClickListener()
//    {
//      @Override public void onClick(View v)
//      {
//        TurboFragment fragment = new TurboFragment();
//        fragment.show(getFragmentManager(),"");
//      }
//    });
//    hands_free_setting.setOnClickListener(new View.OnClickListener()
//    {
//      @Override public void onClick(View v)
//      {
//        SVCFragment fragment = new SVCFragment();
//        fragment.show(getFragmentManager(),"");
//      }
//    });
//    event_history.setOnClickListener(new View.OnClickListener()
//    {
//      @Override public void onClick(View v)
//      {
//        AlligatorFragment fragment = new AlligatorFragment();
//        fragment.show(getFragmentManager(),"");
//      }
//    });
//    schedule_remote_start.setOnClickListener(new View.OnClickListener()
//    {
//      @Override public void onClick(View v)
//      {
//        PasswordFragment fragment = new PasswordFragment();
//        fragment.show(getFragmentManager(),"");
//      }
//    });
//    feature_configuration.setOnClickListener(new View.OnClickListener()
//    {
//      @Override public void onClick(View v)
//      {
//        ChangeButtonFragment fragment = new ChangeButtonFragment();
//        fragment.show(getFragmentManager(),"");
//      }
//    });
//    sensor_adjustment.setOnClickListener(new View.OnClickListener()
//    {
//      @Override public void onClick(View v)
//      {
//        SensorFragment fragment = new SensorFragment();
//        fragment.show(getFragmentManager(),"");
//      }
//    });
//    add_new_phone.setOnClickListener(new View.OnClickListener()
//    {
//      @Override public void onClick(View v)
//      {
//        AddFragment fragment = new AddFragment();
//        fragment.show(getFragmentManager(),"");
//      }
//    });
  }
}
