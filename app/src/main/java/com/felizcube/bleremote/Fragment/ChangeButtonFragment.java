package com.felizcube.bleremote.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.felizcube.bleremote.DragShadowView;
import com.felizcube.bleremote.Main;
import com.felizcube.bleremote.R;

import java.util.ArrayList;


public class ChangeButtonFragment extends DialogFragment
{
  private ImageView back;
  private View view_1;
  private View view_2;
  private RelativeLayout relativeLayout_1;
  private RelativeLayout relativeLayout_2;
  private RelativeLayout relativeLayout_3;
  private RelativeLayout relativeLayout_4;
  private RelativeLayout relativeLayout_5;
  private RelativeLayout relativeLayout_6;
  private ArrayList<ImageView> imageViews = new ArrayList();
  private ArrayList<TextView> textViews = new ArrayList();
  private ImageView iv1;
  private ImageView iv2;
  private ImageView iv3;
  private ImageView iv4;
  private ImageView iv5;
  private ImageView iv6;
  private TextView tv1;
  private TextView tv2;
  private TextView tv3;
  private TextView tv4;
  private TextView tv5;
  private TextView tv6;
  private int drag_target;
  private int drag_destination;

  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    final LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    final Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.page_button_id,container,false);
    InitUI(view);
    InitUIFunction();
    return view;
  }

  private void InitUI(View view)
  {
    back = (ImageView)view.findViewById(R.id.button_id_back);
    view_1 = view.findViewById(R.id.button_id_page_1);
    view_2 = view.findViewById(R.id.button_id_page_2);
    view_1.setBackgroundResource(R.drawable.main_bg_top);
    view_2.setBackgroundResource(R.drawable.main_bg_top);
    relativeLayout_1 = (RelativeLayout)view_1.findViewById(R.id.relative_block_1);
    relativeLayout_2 = (RelativeLayout)view_1.findViewById(R.id.relative_block_2);
    relativeLayout_3 = (RelativeLayout)view_1.findViewById(R.id.relative_block_3);
    relativeLayout_4 = (RelativeLayout)view_2.findViewById(R.id.relative_block_1);
    relativeLayout_5 = (RelativeLayout)view_2.findViewById(R.id.relative_block_2);
    relativeLayout_6 = (RelativeLayout)view_2.findViewById(R.id.relative_block_3);
    iv1 = (ImageView)view_1.findViewById(R.id.iv1);
    iv2 = (ImageView)view_1.findViewById(R.id.iv2);
    iv3 = (ImageView)view_1.findViewById(R.id.iv3);
    iv4 = (ImageView)view_2.findViewById(R.id.iv1);
    iv5 = (ImageView)view_2.findViewById(R.id.iv2);
    iv6 = (ImageView)view_2.findViewById(R.id.iv3);
    tv1 = (TextView)view_1.findViewById(R.id.tv1);
    tv2 = (TextView)view_1.findViewById(R.id.tv2);
    tv3 = (TextView)view_1.findViewById(R.id.tv3);
    tv4 = (TextView)view_2.findViewById(R.id.tv1);
    tv5 = (TextView)view_2.findViewById(R.id.tv2);
    tv6 = (TextView)view_2.findViewById(R.id.tv3);
    imageViews.add(iv1);
    imageViews.add(iv2);
    imageViews.add(iv3);
    imageViews.add(iv4);
    imageViews.add(iv5);
    imageViews.add(iv6);
    textViews.add(tv1);
    textViews.add(tv2);
    textViews.add(tv3);
    textViews.add(tv4);
    textViews.add(tv5);
    textViews.add(tv6);
    UpdateView();
  }

  private void InitUIFunction()
  {
    back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        ChangeButtonFragment.this.dismiss();
      }
    });
    relativeLayout_1.setOnLongClickListener(new View.OnLongClickListener()
    {
      @Override public boolean onLongClick(View v)
      {
        View.DragShadowBuilder myShadow = new DragShadowView(v);
        v.startDrag(null,myShadow,null,0);
        drag_target = 1;
        return false;
      }
    });
    relativeLayout_2.setOnLongClickListener(new View.OnLongClickListener()
    {
      @Override public boolean onLongClick(View v)
      {
        View.DragShadowBuilder myShadow = new DragShadowView(v);
        v.startDrag(null,myShadow,null,0);
        drag_target = 2;
        return false;
      }
    });
    relativeLayout_3.setOnLongClickListener(new View.OnLongClickListener()
    {
      @Override public boolean onLongClick(View v)
      {
        View.DragShadowBuilder myShadow = new DragShadowView(v);
        v.startDrag(null,myShadow,null,0);
        drag_target = 3;
        return false;
      }
    });
    relativeLayout_4.setOnLongClickListener(new View.OnLongClickListener()
    {
      @Override public boolean onLongClick(View v)
      {
        View.DragShadowBuilder myShadow = new DragShadowView(v);
        v.startDrag(null,myShadow,null,0);
        drag_target = 4;
        return false;
      }
    });
    relativeLayout_5.setOnLongClickListener(new View.OnLongClickListener()
    {
      @Override public boolean onLongClick(View v)
      {
        View.DragShadowBuilder myShadow = new DragShadowView(v);
        v.startDrag(null,myShadow,null,0);
        drag_target = 5;
        return false;
      }
    });
    relativeLayout_6.setOnLongClickListener(new View.OnLongClickListener()
    {
      @Override public boolean onLongClick(View v)
      {
        View.DragShadowBuilder myShadow = new DragShadowView(v);
        v.startDrag(null,myShadow,null,0);
        drag_target = 6;
        return false;
      }
    });
    relativeLayout_1.setOnDragListener(new View.OnDragListener()
    {
      @Override public boolean onDrag(View v,DragEvent event)
      {
        switch(event.getAction())
        {
          case DragEvent.ACTION_DROP:
            drag_destination = 1;
            ChangePriority();
            break;
        }
        return true;
      }
    });
    relativeLayout_2.setOnDragListener(new View.OnDragListener()
    {
      @Override public boolean onDrag(View v,DragEvent event)
      {
        switch(event.getAction())
        {
          case DragEvent.ACTION_DROP:
            drag_destination = 2;
            ChangePriority();
            break;
        }
        return true;
      }
    });
    relativeLayout_3.setOnDragListener(new View.OnDragListener()
    {
      @Override public boolean onDrag(View v,DragEvent event)
      {
        switch(event.getAction())
        {
          case DragEvent.ACTION_DROP:
            drag_destination = 3;
            ChangePriority();
            break;
        }
        return true;
      }
    });
    relativeLayout_4.setOnDragListener(new View.OnDragListener()
    {
      @Override public boolean onDrag(View v,DragEvent event)
      {
        switch(event.getAction())
        {
          case DragEvent.ACTION_DROP:
            drag_destination = 4;
            ChangePriority();
            break;
        }
        return true;
      }
    });
    relativeLayout_5.setOnDragListener(new View.OnDragListener()
    {
      @Override public boolean onDrag(View v,DragEvent event)
      {
        switch(event.getAction())
        {
          case DragEvent.ACTION_DROP:
            drag_destination = 5;
            ChangePriority();
            break;
        }
        return true;
      }
    });
    relativeLayout_6.setOnDragListener(new View.OnDragListener()
    {
      @Override public boolean onDrag(View v,DragEvent event)
      {
        switch(event.getAction())
        {
          case DragEvent.ACTION_DROP:
            drag_destination = 6;
            ChangePriority();
            break;
        }
        return true;
      }
    });
  }

  private void ChangePriority()
  {
    String tmp_target = Main.data.priority.get(drag_target-1).toString();
    String tmp_destination = Main.data.priority.get(drag_destination-1).toString();
    Main.data.priority.set(drag_target-1,tmp_destination);
    Main.data.priority.set(drag_destination-1,tmp_target);
    UpdateView();
    Main.data.SaveData();
  }

  private void UpdateView()
  {
    /*update this fragment*/
    for(int i = 0 ; i < textViews.size() ; i++)
    {
      textViews.get(i).setText(Main.data.priority.get(i).toString());
    }
    Main.UpdateData();
  }

}
