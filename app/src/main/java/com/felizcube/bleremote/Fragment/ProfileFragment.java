package com.felizcube.bleremote.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.felizcube.bleremote.Main;
import com.felizcube.bleremote.R;
import com.felizcube.bleremote.Tool;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.squareup.picasso.Picasso;

import java.io.File;

import static android.app.Activity.RESULT_OK;
import static com.felizcube.bleremote.Main.metrics;

public class ProfileFragment extends DialogFragment
{
  private static ImageView photo;
  private static Activity activity;
  private ImageView back;
  private static LinearLayout car_add_block_1;
  private static LinearLayout car_add_block_2;
  private static LinearLayout car_add_block_3;
  private static LinearLayout car_add_block_4;
  private static ImageView save;
  private static EditText name;
  private static TextView name1;
  private static TextView name2;
  private static TextView name3;
  private static TextView name4;
  private static ImageView delete1;
  private static ImageView delete2;
  private static ImageView delete3;
  private static ImageView delete4;
  private ImageChooserManager image_chooser_manager;
  private ImageChooserListener ICL;
  private static String image_path = "";
  private static boolean add_mode = false;
  private static final int REQUEST_EXTERNAL_STORAGE = 1;
  private static String[] PERMISSIONS_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE,
          Manifest.permission.WRITE_EXTERNAL_STORAGE};


  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    activity = getActivity();
    View view = inflater.inflate(R.layout.page_cars_profile,container,false);
    ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE,
            REQUEST_EXTERNAL_STORAGE);
    InitUI(view);
    InitUIFunction();
    ICL = new ImageChooserListener()
    {
      @Override public void onImageChosen(final ChosenImage image)
      {
        if(image != null)
        {
          LoadImage(image.getFilePathOriginal());
        }
      }

      @Override public void onError(final String s)
      {
        Tool.LOG("OnError: " + s);
      }

      @Override public void onImagesChosen(final ChosenImages images)
      {

      }
    };
    /*take photo*/
    StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
    StrictMode.setVmPolicy(builder.build());
    return view;
  }

  private void InitUI(View view)
  {
    back = (ImageView)view.findViewById(R.id.profile_back);
    save = (ImageView)view.findViewById(R.id.profile_save);
    photo = (ImageView)view.findViewById(R.id.profile_photo);
    name = (EditText)view.findViewById(R.id.profile_name);
    name1 = (TextView)view.findViewById(R.id.profile_name_1);
    name2 = (TextView)view.findViewById(R.id.profile_name_2);
    name3 = (TextView)view.findViewById(R.id.profile_name_3);
    name4 = (TextView)view.findViewById(R.id.profile_name_4);
    delete1 = (ImageView)view.findViewById(R.id.profile_delete_1);
    delete2 = (ImageView)view.findViewById(R.id.profile_delete_2);
    delete3 = (ImageView)view.findViewById(R.id.profile_delete_3);
    delete4 = (ImageView)view.findViewById(R.id.profile_delete_4);
    car_add_block_1 = (LinearLayout)view.findViewById(R.id.car_add_block_1);
    car_add_block_2 = (LinearLayout)view.findViewById(R.id.car_add_block_2);
    car_add_block_3 = (LinearLayout)view.findViewById(R.id.car_add_block_3);
    car_add_block_4 = (LinearLayout)view.findViewById(R.id.car_add_block_4);
    UpdateUI();
  }

  private void InitUIFunction()
  {
    back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        ProfileFragment.this.dismiss();
      }
    });
    name1.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        if(name1.getText().toString().equals(getString(R.string.profile_add)))
        {
          AddNewCar();
        }
        else
        {
          Main.car_data.SelectCar(0);
          add_mode = false;
          UpdateUI();
        }
      }
    });
    name2.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        if(name2.getText().toString().equals(getString(R.string.profile_add)))
        {
          AddNewCar();
        }
        else
        {
          Main.car_data.SelectCar(1);
          add_mode = false;
          UpdateUI();
        }
      }
    });
    name3.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        if(name3.getText().toString().equals(getString(R.string.profile_add)))
        {
          AddNewCar();
        }
        else
        {
          Main.car_data.SelectCar(2);
          add_mode = false;
          UpdateUI();
        }
      }
    });
    name4.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        if(name4.getText().toString().equals(getString(R.string.profile_add)))
        {
          AddNewCar();
        }
        else
        {
          Main.car_data.SelectCar(3);
          add_mode = false;
          UpdateUI();
        }
      }
    });
    delete1.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        DeleteFragment fragment = new DeleteFragment();
        fragment.show(getFragmentManager(),"0");
      }
    });
    delete2.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        DeleteFragment fragment = new DeleteFragment();
        fragment.show(getFragmentManager(),"1");
      }
    });
    delete3.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        DeleteFragment fragment = new DeleteFragment();
        fragment.show(getFragmentManager(),"2");
      }
    });
    delete4.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        DeleteFragment fragment = new DeleteFragment();
        fragment.show(getFragmentManager(),"3");
      }
    });
    photo.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        alertDialog.getWindow().setLayout(metrics.widthPixels / 5 * 4,metrics.heightPixels / 2);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.page_select_picture,null);
        alertDialog.getWindow().setContentView(dialogView);
        ImageView back = (ImageView)dialogView.findViewById(R.id.select_back);
        TextView picture = (TextView)dialogView.findViewById(R.id.select_picture);
        TextView photo = (TextView)dialogView.findViewById(R.id.select_photo);
        back.setOnClickListener(new View.OnClickListener()
        {
          @Override public void onClick(View v)
          {
            alertDialog.dismiss();
          }
        });
        picture.setOnClickListener(new View.OnClickListener()
        {
          @Override public void onClick(View v)
          {
            ChooseImage(ChooserType.REQUEST_PICK_PICTURE);
            alertDialog.dismiss();
          }
        });
        photo.setOnClickListener(new View.OnClickListener()
        {
          @Override public void onClick(View v)
          {
            ChooseImage(ChooserType.REQUEST_CAPTURE_PICTURE);
            alertDialog.dismiss();
          }
        });
      }
    });
    name.addTextChangedListener(new TextWatcher()
    {
      @Override public void beforeTextChanged(CharSequence s,int start,int count,int after)
      {
      }

      @Override public void onTextChanged(CharSequence s,int start,int before,int count)
      {
        if(! add_mode)
        {
          save.setVisibility(View.VISIBLE);
        }
      }

      @Override public void afterTextChanged(Editable s)
      {
      }
    });
    save.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        Main.car_data.ChangeCarName(name.getText().toString());
        UpdateUI();
      }
    });
  }

  public static void UpdateUI()
  {
    LoadImage("");
    name.setText("");
    car_add_block_2.setVisibility(View.INVISIBLE);
    car_add_block_3.setVisibility(View.INVISIBLE);
    car_add_block_4.setVisibility(View.INVISIBLE);
    if(Main.car_data.cars.size() == 0)
    {
      add_mode = true;
    }
    if(add_mode)
    {
      car_add_block_1.setBackgroundColor(Color.BLUE);
      car_add_block_2.setBackgroundColor(Color.BLUE);
      car_add_block_3.setBackgroundColor(Color.BLUE);
      car_add_block_4.setBackgroundColor(Color.BLUE);
    }
    else
    {
      car_add_block_1.setBackgroundColor(Color.WHITE);
      car_add_block_2.setBackgroundColor(Color.WHITE);
      car_add_block_3.setBackgroundColor(Color.WHITE);
      car_add_block_4.setBackgroundColor(Color.WHITE);
    }
    name1.setText(R.string.profile_add);
    name2.setText(R.string.profile_add);
    name3.setText(R.string.profile_add);
    name4.setText(R.string.profile_add);
    delete1.setVisibility(View.GONE);
    delete2.setVisibility(View.GONE);
    delete3.setVisibility(View.GONE);
    delete4.setVisibility(View.GONE);
    if(Main.car_data.cars.size() > 0)
    {
      name1.setText(Main.car_data.cars.get(0).name);
      delete1.setVisibility(View.VISIBLE);
      car_add_block_1.setBackgroundColor(Color.WHITE);
      car_add_block_2.setVisibility(View.VISIBLE);
      if(Main.car_data.cars.get(0).select == 1)
      {
        name.setText(Main.car_data.cars.get(0).name);
        LoadImage(Main.car_data.cars.get(0).photo);
      }
    }
    if(Main.car_data.cars.size() > 1)
    {
      name2.setText(Main.car_data.cars.get(1).name);
      delete2.setVisibility(View.VISIBLE);
      car_add_block_2.setBackgroundColor(Color.WHITE);
      car_add_block_3.setVisibility(View.VISIBLE);
      if(Main.car_data.cars.get(1).select == 1)
      {
        name.setText(Main.car_data.cars.get(1).name);
        LoadImage(Main.car_data.cars.get(1).photo);
      }
    }
    if(Main.car_data.cars.size() > 2)
    {
      name3.setText(Main.car_data.cars.get(2).name);
      delete3.setVisibility(View.VISIBLE);
      car_add_block_3.setBackgroundColor(Color.WHITE);
      car_add_block_4.setVisibility(View.VISIBLE);
      if(Main.car_data.cars.get(2).select == 1)
      {
        name.setText(Main.car_data.cars.get(2).name);
        LoadImage(Main.car_data.cars.get(2).photo);
      }
    }
    if(Main.car_data.cars.size() > 3)
    {
      name4.setText(Main.car_data.cars.get(3).name);
      delete4.setVisibility(View.VISIBLE);
      car_add_block_4.setBackgroundColor(Color.WHITE);
      if(Main.car_data.cars.get(3).select == 1)
      {
        name.setText(Main.car_data.cars.get(3).name);
        LoadImage(Main.car_data.cars.get(3).photo);
      }
    }
    save.setVisibility(View.INVISIBLE);
    name.clearFocus();
  }

  private void AddNewCar()
  {
    if(add_mode)
    {
      if(name.getText().toString().length() == 0)
      {
        name.setText("");
        name.setHint(R.string.profile_car_edit_name);
        name.setHintTextColor(Color.RED);
      }
      else
      {
        if(Main.car_data.IsSameName(name.getText().toString()))
        {
          name.setText("");
          name.setHint(R.string.profile_car_same_name);
          name.setHintTextColor(Color.RED);
        }
        else
        {
          Main.car_data.AddNewCar(name.getText().toString(),image_path);
          add_mode = false;
          UpdateUI();
        }
      }
    }
    else
    {
      add_mode = true;
      Main.car_data.RemoveSelectCar();
      UpdateUI();
    }
  }

  public static void DeleteCar(int index)
  {
    Main.car_data.DeleteCar(index);
    UpdateUI();
  }

  private void ChooseImage(int type)
  {
    image_chooser_manager = new ImageChooserManager(this,type,true);
    image_chooser_manager.setImageChooserListener(ICL);
    image_chooser_manager.clearOldFiles();
    try
    {
      image_chooser_manager.choose();
    }
    catch(Exception e)
    {
      Tool.LOG(e.getMessage());
    }
  }

  private static void LoadImage(final String path)
  {
    activity.runOnUiThread(new Runnable()
    {
      @Override public void run()
      {
        Picasso.with(activity).load(Uri.fromFile(new File(path))).into(photo);
        image_path = path;
        if(Main.car_data.GetSelectCarIndex() != - 1 && image_path.length() != 0)
        {
          Main.car_data.AddPhoto(Main.car_data.GetSelectCarIndex(),image_path);
        }
      }
    });
  }

  @Override public void onActivityResult(int requestCode,int resultCode,Intent data)
  {
    if(resultCode == RESULT_OK && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE))
    {
      image_chooser_manager.submit(requestCode,data);
    }
  }
}
