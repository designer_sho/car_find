package com.felizcube.bleremote.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.felizcube.bleremote.DataRestore.UIData;
import com.felizcube.bleremote.Main;
import com.felizcube.bleremote.R;
import com.felizcube.bleremote.Tool;


public class PasswordFragment extends DialogFragment
{
  private ImageView back;
  private ToggleButton toggle_switch;
  private TextView password_old;
  private TextView password_1;
  private TextView password_2;
  private TextView password_3;
  private TextView password_4;
  private TextView password_hint;
  private Button button_1;
  private Button button_2;
  private Button button_3;
  private Button button_4;
  private Button button_5;
  private Button button_6;
  private Button button_7;
  private Button button_8;
  private Button button_9;
  private Button button_ok;
  private Button button_0;
  private Button button_back;
  private String password = "";
  private int password_mode = 0;

  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    final LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    final Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    if(Main.password_lock)
    {
      password_mode = 2;
      dialog.setOnKeyListener(keylistener);
      dialog.setCancelable(false);
    }
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.page_password,container,false);
    InitUI(view);
    InitUIFunction();
    return view;
  }

  private void InitUI(View view)
  {
    back = (ImageView)view.findViewById(R.id.password_back);
    toggle_switch = (ToggleButton)view.findViewById(R.id.password_switch);
    password_old = (TextView)view.findViewById(R.id.password_old);
    password_1 = (TextView)view.findViewById(R.id.password_1);
    password_2 = (TextView)view.findViewById(R.id.password_2);
    password_3 = (TextView)view.findViewById(R.id.password_3);
    password_4 = (TextView)view.findViewById(R.id.password_4);
    password_hint = (TextView)view.findViewById(R.id.password_hint);
    button_1 = (Button)view.findViewById(R.id.password_button_1);
    button_2 = (Button)view.findViewById(R.id.password_button_2);
    button_3 = (Button)view.findViewById(R.id.password_button_3);
    button_4 = (Button)view.findViewById(R.id.password_button_4);
    button_5 = (Button)view.findViewById(R.id.password_button_5);
    button_6 = (Button)view.findViewById(R.id.password_button_6);
    button_7 = (Button)view.findViewById(R.id.password_button_7);
    button_8 = (Button)view.findViewById(R.id.password_button_8);
    button_9 = (Button)view.findViewById(R.id.password_button_9);
    button_ok = (Button)view.findViewById(R.id.password_button_ok);
    button_0 = (Button)view.findViewById(R.id.password_button_0);
    button_back = (Button)view.findViewById(R.id.password_button_back);
    if(Main.data.code.length() == 0 && password_mode != 2)
    {
      password_old.setText(R.string.password_toast_input_new_password);
      password_mode = 1;
    }
    if(password_mode == 2)
    {
      password_old.setText(R.string.password_toast_enter_password);
      toggle_switch.setVisibility(View.INVISIBLE);
    }
    toggle_switch.setChecked(Main.data.enable_password);
  }

  private void InitUIFunction()
  {
    back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        PasswordFragment.this.dismiss();
      }
    });
    toggle_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      @Override public void onCheckedChanged(CompoundButton buttonView,boolean isChecked)
      {
        Tool.LOG(isChecked + "");
        Main.data.enable_password = isChecked;
        UIData.SaveData();
      }
    });
    button_1.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("1");
      }
    });
    button_2.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("2");
      }
    });
    button_3.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("3");
      }
    });
    button_4.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("4");
      }
    });
    button_5.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("5");
      }
    });
    button_6.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("6");
      }
    });
    button_7.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("7");
      }
    });
    button_8.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("8");
      }
    });
    button_9.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("9");
      }
    });
    button_ok.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("ok");
      }
    });
    button_0.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("0");
      }
    });
    button_back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        InputPassword("back");
      }
    });
  }

  private void InputPassword(String type)
  {
    if(type == "ok")
    {
      if(password.length() < 4)
      {
        password_hint.setText(R.string.password_toast_password_short);
      }
      else
      {
        if(password_mode == 0 && Main.data.code.length() != 0)
        {
          if(password.equals(Main.data.code))
          {
            password_mode = 1;
            password_old.setText(R.string.password_toast_input_new_password);
            password = "";
            UpdatePassword();
          }
          else
          {
            password_hint.setText(R.string.password_toast_password_wrong);
          }
        }
        else if(password_mode == 1)
        {
          password_hint.setText(R.string.password_toast_save_password);
          Main.data.code = password;
          UIData.SaveData();
        }
        else if(password_mode == 2)
        {
          if(password.equals(Main.data.code))
          {
            Main.password_lock = false;
            PasswordFragment.this.dismiss();
          }
          else
          {
            password_hint.setText(R.string.password_toast_password_wrong);
          }
        }
      }
    }
    else if(type == "back")
    {
      if(password.length() < 1)
      {
        return;
      }
      password = password.substring(0,password.length() - 1);
      UpdatePassword();
    }
    else
    {
      if(password.length() >= 4)
      {
        return;
      }
      password = password + type;
      UpdatePassword();
    }
  }

  private void UpdatePassword()
  {
    password_hint.setText("");
    if(password.length() > 0)
    {
      password_1.setText("" + password.charAt(0));
    }
    else
    {
      password_1.setText("-");
    }
    if(password.length() > 1)
    {
      password_2.setText("" + password.charAt(1));
    }
    else
    {
      password_2.setText("-");
    }
    if(password.length() > 2)
    {
      password_3.setText("" + password.charAt(2));
    }
    else
    {
      password_3.setText("-");
    }
    if(password.length() > 3)
    {
      password_4.setText("" + password.charAt(3));
    }
    else
    {
      password_4.setText("-");
    }
  }

  private DialogInterface.OnKeyListener keylistener = new DialogInterface.OnKeyListener()
  {
    public boolean onKey(DialogInterface dialog,int keyCode,KeyEvent event)
    {
      if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  };
}
