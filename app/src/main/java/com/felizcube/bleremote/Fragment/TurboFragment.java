package com.felizcube.bleremote.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import com.felizcube.bleremote.R;
import com.felizcube.bleremote.Tool;

public class TurboFragment extends DialogFragment
{
  private ImageView back;
  private NumberPicker mode;
  private LinearLayout save;

  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    final LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    final Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.page_turbo,container,false);
    InitUI(view);
    InitUIFunction();
    return view;
  }

  private void InitUI(View view)
  {
    back = (ImageView)view.findViewById(R.id.turbo_back);
    mode = (NumberPicker)view.findViewById(R.id.turbo_mode_select);
    save = (LinearLayout)view.findViewById(R.id.turbo_save);
    String[] values = new String[5];
    for(int i = 0 ; i < values.length ; i++)
    {
      if(i == 0)
      {
        values[i] = "OFF";
      }
      else
      {
        values[i] = i + "";
      }
    }
    mode.setMaxValue(values.length - 1);
    mode.setMinValue(0);
    mode.setDisplayedValues(values);
    mode.setValue(2);
    mode.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    mode.setWrapSelectorWheel(false);
  }

  private void InitUIFunction()
  {
    back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        TurboFragment.this.dismiss();
      }
    });
    save.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        Tool.LOG("" + mode.getValue());
        TurboFragment.this.dismiss();
      }
    });
  }
}
