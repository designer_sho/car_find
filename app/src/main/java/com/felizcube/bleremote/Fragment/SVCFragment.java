package com.felizcube.bleremote.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.felizcube.bleremote.Main;
import com.felizcube.bleremote.R;
import com.felizcube.bleremote.Tool;

public class SVCFragment extends DialogFragment
{
  private ImageView back;
  private ToggleButton toggle_switch;
  private static LinearLayout block_1;
  private static LinearLayout block_2;
  private static LinearLayout block_3;
  private static LinearLayout block_4;
  private static TextView name1;
  private static TextView name2;
  private static TextView name3;
  private static TextView name4;
  private static ImageView delete1;
  private static ImageView delete2;
  private static ImageView delete3;
  private static ImageView delete4;
  private SeekBar seekbar;
  private LinearLayout save;
  private static boolean add_mode = false;

  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    final LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    final Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.page_svc,container,false);
    InitUI(view);
    InitUIFunction();
    return view;
  }

  private void InitUI(View view)
  {
    back = (ImageView)view.findViewById(R.id.svc_back);
    toggle_switch = (ToggleButton)view.findViewById(R.id.svc_switch);
    block_1 = (LinearLayout)view.findViewById(R.id.svc_block_1);
    block_2 = (LinearLayout)view.findViewById(R.id.svc_block_2);
    block_3 = (LinearLayout)view.findViewById(R.id.svc_block_3);
    block_4 = (LinearLayout)view.findViewById(R.id.svc_block_4);
    name1 = (TextView)view.findViewById(R.id.svc_name_1);
    name2 = (TextView)view.findViewById(R.id.svc_name_2);
    name3 = (TextView)view.findViewById(R.id.svc_name_3);
    name4 = (TextView)view.findViewById(R.id.svc_name_4);
    delete1 = (ImageView)view.findViewById(R.id.svc_delete_1);
    delete2 = (ImageView)view.findViewById(R.id.svc_delete_2);
    delete3 = (ImageView)view.findViewById(R.id.svc_delete_3);
    delete4 = (ImageView)view.findViewById(R.id.svc_delete_4);
    seekbar = (SeekBar)view.findViewById(R.id.svc_seekbar);
    save = (LinearLayout)view.findViewById(R.id.svc_save);
    UpdateUI();
  }

  private void InitUIFunction()
  {
    back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        SVCFragment.this.dismiss();
      }
    });
    toggle_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
    {
      @Override public void onCheckedChanged(CompoundButton buttonView,boolean isChecked)
      {
        Tool.LOG(isChecked+"");
      }
    });
    save.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        Tool.LOG("" + seekbar.getProgress());
      }
    });
  }

  public static void UpdateUI()
  {
    Tool.LOG(""+ Main.svc_data.svcs.size());
    block_2.setVisibility(View.INVISIBLE);
    block_3.setVisibility(View.INVISIBLE);
    block_4.setVisibility(View.INVISIBLE);
    if(Main.svc_data.svcs.size() == 0)
    {
      add_mode = true;
    }
    if(add_mode)
    {
      block_1.setBackgroundColor(Color.BLUE);
      block_2.setBackgroundColor(Color.BLUE);
      block_3.setBackgroundColor(Color.BLUE);
      block_4.setBackgroundColor(Color.BLUE);
    }
    else
    {
      block_1.setBackgroundColor(Color.WHITE);
      block_2.setBackgroundColor(Color.WHITE);
      block_3.setBackgroundColor(Color.WHITE);
      block_4.setBackgroundColor(Color.WHITE);
    }
    name1.setText(R.string.svc_add);
    name2.setText(R.string.svc_add);
    name3.setText(R.string.svc_add);
    name4.setText(R.string.svc_add);
    delete1.setVisibility(View.GONE);
    delete2.setVisibility(View.GONE);
    delete3.setVisibility(View.GONE);
    delete4.setVisibility(View.GONE);
//    if(Main.svc_data.svcs.size() > 0)
//    {
//      name1.setText(Main.svc_data.svcs.get(0).name);
//      delete1.setVisibility(View.VISIBLE);
//      car_add_block_1.setBackgroundColor(Color.WHITE);
//      car_add_block_2.setVisibility(View.VISIBLE);
//      if(Main.car_data.cars.get(0).select == 1)
//      {
//        name.setText(Main.car_data.cars.get(0).name);
//        LoadImage(Main.car_data.cars.get(0).photo);
//      }
//    }
//    if(Main.car_data.cars.size() > 1)
//    {
//      name2.setText(Main.car_data.cars.get(1).name);
//      delete2.setVisibility(View.VISIBLE);
//      car_add_block_2.setBackgroundColor(Color.WHITE);
//      car_add_block_3.setVisibility(View.VISIBLE);
//      if(Main.car_data.cars.get(1).select == 1)
//      {
//        name.setText(Main.car_data.cars.get(1).name);
//        LoadImage(Main.car_data.cars.get(1).photo);
//      }
//    }
//    if(Main.car_data.cars.size() > 2)
//    {
//      name3.setText(Main.car_data.cars.get(2).name);
//      delete3.setVisibility(View.VISIBLE);
//      car_add_block_3.setBackgroundColor(Color.WHITE);
//      car_add_block_4.setVisibility(View.VISIBLE);
//      if(Main.car_data.cars.get(2).select == 1)
//      {
//        name.setText(Main.car_data.cars.get(2).name);
//        LoadImage(Main.car_data.cars.get(2).photo);
//      }
//    }
//    if(Main.car_data.cars.size() > 3)
//    {
//      name4.setText(Main.car_data.cars.get(3).name);
//      delete4.setVisibility(View.VISIBLE);
//      car_add_block_4.setBackgroundColor(Color.WHITE);
//      if(Main.car_data.cars.get(3).select == 1)
//      {
//        name.setText(Main.car_data.cars.get(3).name);
//        LoadImage(Main.car_data.cars.get(3).photo);
//      }
//    }
  }
}
