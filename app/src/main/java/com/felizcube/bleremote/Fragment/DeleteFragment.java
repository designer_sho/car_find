package com.felizcube.bleremote.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.felizcube.bleremote.Main;
import com.felizcube.bleremote.R;
import com.felizcube.bleremote.Tool;

import static com.felizcube.bleremote.Main.metrics;

public class DeleteFragment extends DialogFragment
{
  private TextView yes ;
  private TextView no ;
  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(metrics.widthPixels / 5 * 4,metrics.heightPixels / 2));
    Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(metrics.widthPixels / 5 * 4,metrics.heightPixels / 2);
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.page_delete,container,false);
    InitUI(view);
    InitUIFunction();
    return view;
  }

  private void InitUI(View view)
  {
    yes = (TextView)view.findViewById(R.id.delete_yes);
    no = (TextView)view.findViewById(R.id.delete_no);
  }

  private void InitUIFunction()
  {
    no.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        DeleteFragment.this.dismiss();
      }
    });
    yes.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        DeleteFragment.this.dismiss();
        ProfileFragment.DeleteCar(Integer.parseInt(DeleteFragment.this.getTag()));
      }
    });
  }
}
