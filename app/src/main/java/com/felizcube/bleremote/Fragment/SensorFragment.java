package com.felizcube.bleremote.Fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.felizcube.bleremote.R;
import com.felizcube.bleremote.Tool;

public class SensorFragment extends DialogFragment
{
  private ImageView back;
  private SeekBar shock_seekbar;
  private LinearLayout shock_save;
  private SeekBar tilt_seekbar;
  private LinearLayout tilt_save;

  @Override public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    final LinearLayout root = new LinearLayout(getActivity());
    root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    final Dialog dialog = new Dialog(getActivity());
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(root);
    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    return dialog;
  }

  public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
  {
    View view = inflater.inflate(R.layout.page_sensor,container,false);
    InitUI(view);
    InitUIFunction();
    return view;
  }

  private void InitUI(View view)
  {
    back = (ImageView)view.findViewById(R.id.sensor_back);
    shock_seekbar = (SeekBar)view.findViewById(R.id.sensor_shock_seekbar);
    shock_save = (LinearLayout)view.findViewById(R.id.sensor_shock_save);
    tilt_seekbar = (SeekBar)view.findViewById(R.id.sensor_tilt_seekbar);
    tilt_save = (LinearLayout)view.findViewById(R.id.sensor_tilt_save);
  }

  private void InitUIFunction()
  {
    back.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        SensorFragment.this.dismiss();
      }
    });
    shock_save.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        Tool.LOG("" + shock_seekbar.getProgress());
      }
    });
    tilt_save.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        Tool.LOG("" + tilt_seekbar.getProgress());
      }
    });
  }
}
