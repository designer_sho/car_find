package com.felizcube.bleremote;

import android.bluetooth.le.ScanResult;
import android.view.View;

import java.util.List;

/**
 * Created by LutherHsieh on 2017/7/7.
 */

public interface BTVehicleHandler
{
  public void addResult(ScanResult result);

  public void saveVehicleToDB(VehicleObj vObj);

  public void updateVehicleView(VehicleObj vObj);

  public boolean isBTHanlderBusy();

  public void notifyAuthedVehicle(VehicleObj vObj);

  public void notifyCommandTimeout();

  public void notifyConnected();

  public void notifyBTError();

}
