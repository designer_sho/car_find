package com.felizcube.bleremote;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by LutherHsieh on 2017/7/7.
 */

public class DataObj
{
  private static final String TAG = DataObj.class.getSimpleName();

  /*是否啟動 震動功能?*/
  public boolean IsEnableVibrator = true;
  public static int VIBRATE_TIME = 100;

  private static DataObj mInstance = null;
  private SharedPreferences mSetting = null;
  private Activity mNowActivity = null;
  private static final String SETTING_NAME = "prj_010_gwell_blevehicleguard_android";

  private static final String KEY_EN_VIBRATOR = "EnVibrator";
  private static final String KEY_LINK_VEHICLE = "LinkVehicle";

  private VehicleObj mLinkedVehicle = null;
  private ArrayList<VehicleObj> mVehicleExistList;
  private ArrayList<VehicleObj> mVehicleAuthedList;

  private static ObjectMapper gObjMapper = new ObjectMapper();

  /**
   * 記錄 工作 Queue
   */
  private Queue<BleCmdObj> mCmdQueue;
  private Object mLockObj = new Object();

  private DataObj()
  {
    mVehicleExistList = new ArrayList<>();
    mVehicleAuthedList = new ArrayList<>();

    mCmdQueue = new LinkedList<>();
  }

  public boolean isJobCanAdd(BleCmdObj cmd)
  {
    boolean isSkip = false;
    BleCmdObj cmdObj = null;
    if (cmd == null)
    {
      return isSkip;
    }
    /*單純比對 mRunningBleCmd */
    if (mRunningBleCmd != null)
    {
      if ( (cmd.cmd == mRunningBleCmd.cmd ) &&
             (cmd.cmd == BleCmdObj.CMD_VEHICLE_RECONNECT))
             //((cmd.cmd == BleCmdObj.CMD_VEHICLE_AUTH) || (cmd.cmd == BleCmdObj.CMD_VEHICLE_RECONNECT)))
      {
        isSkip = cmd.vehicleObj.equals(mRunningBleCmd.vehicleObj.mac);
      }
      else if (cmd.cmd == BleCmdObj.CMD_VEHICLE_TEST)
      {
        isSkip = true;
      }
    }

    //if ((cmd.cmd == BleCmdObj.CMD_VEHICLE_AUTH) ||
    if (cmd.cmd == BleCmdObj.CMD_VEHICLE_RECONNECT)
    {
      String mac = cmd.vehicleObj.mac;
      synchronized (mLockObj)
      {
        /*檢查  auth 不重複*/
        Iterator<BleCmdObj> it = mCmdQueue.iterator();
        while (it.hasNext())
        {
          cmdObj = it.next();
          if (cmdObj.cmd == cmd.cmd &&
            mac.equals(cmdObj.vehicleObj.mac))
          {
            isSkip = true;
            break;
          }
        }
      }
    }
    else if (cmd.cmd == BleCmdObj.CMD_VEHICLE_TEST)
    {
      synchronized (mLockObj)
      {
        /* 測試命令 只保留一個*/
        Iterator<BleCmdObj> it = mCmdQueue.iterator();
        while (it.hasNext())
        {
          cmdObj = it.next();
          if (cmdObj.cmd == BleCmdObj.CMD_VEHICLE_TEST)
          {
            isSkip = true;
            break;
          }
        }
      }
    }

    return !isSkip;
  }

  public BleCmdObj mRunningBleCmd = null;
  public void addJobCmd(BleCmdObj cmd)
  {
    addJobCmd(cmd, false);
  }

  public void addJobCmd(BleCmdObj cmd, boolean isForceAdd)
  {
    if (isForceAdd || isJobCanAdd(cmd))
    {
      mCmdQueue.offer(cmd);
    }
    else if (cmd != null)
    {
      Log.d(TAG, String.format("Skip Cmd:%s", BleCmdObj.getCommandName(cmd.cmd)));
    }
  }

  public boolean hasJob()
  {
    return (mCmdQueue.size() > 0);
  }

  public void resetJob()
  {
    synchronized (mLockObj)
    {
      mCmdQueue.clear();
    }
  }


  public BleCmdObj getJob()
  {
    BleCmdObj res = null;
    synchronized (mLockObj)
    {
      res = mCmdQueue.poll();
    }
    return res;
  }



  public VehicleObj getVehicleByMacInExistList(String mac)
  {
    return getVehicleByMacInList(mac, mVehicleExistList);
  }

  public ArrayList<VehicleObj> getVehicleAuthedList()
  {
    return mVehicleAuthedList;
  }



  public VehicleObj getVehicleByMacInList(String mac, List<VehicleObj> list)
  {
    VehicleObj res = null;
    if (LUtil.isStrNull(mac) || list == null)
    {
      return res;
    }

    for (VehicleObj batObj : list)
    {
      if (batObj.mac.equals((mac)))
      {
        res = batObj;
        break;
      }
    }
    return res;
  }

  public int getVehicleIndexByMacInExistList(String mac)
  {
    return getVehicleIndexByMacInList(mac, mVehicleExistList);
  }
  public int getVehicleIndexByMacInAuthedList(String mac)
  {
    return getVehicleIndexByMacInList(mac, mVehicleAuthedList);
  }


  public int getVehicleIndexByMacInList(String mac, List<VehicleObj> list)
  {
    int res = -1;
    if (LUtil.isStrNull(mac) || list == null)
    {
      return res;
    }

    for (int i=list.size()-1;i>=0;i--)
    {
      if (list.get(i).mac.equals(mac))
      {
        res = i;
        break;
      }
    }

    return res;
  }

  public boolean addVehicle(VehicleObj batObj)
  {
    boolean res = false;
    if (batObj != null && !LUtil.isStrNull(batObj.mac))
    {
      if (getVehicleIndexByMacInExistList(batObj.mac) < 0)
      {
        mVehicleExistList.add(batObj);
        res = true;
      }
    }
    return res;
  }

  public boolean addAuthedVehicle(VehicleObj batObj)
  {
    boolean res = false;
    if (batObj != null && !LUtil.isStrNull(batObj.mac))
    {
      if (getVehicleIndexByMacInAuthedList(batObj.mac) < 0)
      {
        mVehicleAuthedList.add(batObj);
        res = true;
      }
    }
    return res;
  }

  public void resetVehicleExistAndAuthList()
  {
    mVehicleExistList.clear();
    mVehicleAuthedList.clear();
  }



  /**
   * 取得 Instance
   * @return
   */
  public static DataObj getInstance()
  {
    if (mInstance == null)
    {
      mInstance = new DataObj();
    }
    return mInstance;
  }

  /**
   * 初始化, 需要指定 Activity, 用於取得 Setting File.
   * @param act
   */
  public void init(Activity act)
  {
    if (act == null)
    {
      return;
    }
    mNowActivity = act;
    mSetting = act.getSharedPreferences(SETTING_NAME, 0);

    IsEnableVibrator = mSetting.getBoolean(KEY_EN_VIBRATOR,true);
    String mac = mSetting.getString(KEY_LINK_VEHICLE, "");
    if (!LUtil.isStrNull(mac))
    {
      VehicleObj batObj = new VehicleObj();
      batObj.mac = mac;
      batObj.authState = VehicleObj.AUTH_PASS;
      mLinkedVehicle = batObj;
      /*此時 還不需要連線.*/
      /*加入已認證清單*/
      addAuthedVehicle(mLinkedVehicle);
      addVehicle(mLinkedVehicle);
    }
  }

  public void ClearAllShared()
  {
    mSetting.edit().clear().commit();
  }


  /**
   * 把連線資訊 複製給 batObj
   * @param batObj
   */
  public void copyLinkInfo(VehicleObj batObj)
  {
    if (mLinkedVehicle != null)
    {
      batObj.mac = mLinkedVehicle.mac;
      batObj.devName = mLinkedVehicle.devName;
      if (batObj.rssi != 0)
      {
        mLinkedVehicle.rssi = batObj.rssi;
      }
    }


  }


  public void saveVehicleDisplayName(VehicleObj batObj, String name)
  {
    if (batObj != null)
    {
      mSetting.edit().putString(batObj.mac, name).commit();
    }
  }

  public void removeVehicleDisplayName(VehicleObj batObj)
  {
    if (batObj != null)
    {
      mSetting.edit().remove(batObj.mac).commit();
    }
  }


  public String getVehicleDisplayName(VehicleObj batObj)
  {
    String res = null;
    if (batObj != null)
    {
      res = mSetting.getString(batObj.mac, "");
      if (LUtil.isStrNull(res))
      {
        res = batObj.devName;
      }
    }
    return res;
  }



  /**
   * 設定 vibrator, 並儲存
   * @param isEn
   */
  public void setVibrator(boolean isEn)
  {
    mSetting.edit().putBoolean(KEY_EN_VIBRATOR, isEn ).commit();
    IsEnableVibrator = isEn;
  }

  public void setLinkedVehicle(VehicleObj batObj)
  {
    if (batObj == null)
    {
      mSetting.edit().remove(KEY_LINK_VEHICLE).commit();
      mLinkedVehicle = null;
    }
    else
    {
      mSetting.edit().putString(KEY_LINK_VEHICLE, batObj.mac).commit();
      mLinkedVehicle = batObj;
    }
  }

  public VehicleObj getLinkedVehicle()
  {
    return mLinkedVehicle;
  }




}
