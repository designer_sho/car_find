package com.felizcube.bleremote;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.felizcube.bleremote.Fragment.SettingFragment;

public class Demo extends Activity
{
  private ImageView setting;



  private View mDecorView = null;

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    /*讓畫面全螢幕, 包含 Home Key 在螢幕上的手機*/
    if (hasFocus) {
      mDecorView.setSystemUiVisibility(
        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
          | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
          | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
          | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
          | View.SYSTEM_UI_FLAG_FULLSCREEN
          | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
  }

  @Override protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_demo);

    mDecorView = getWindow().getDecorView();

    setting = (ImageView)findViewById(R.id.setting_button);
    setting.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        SettingFragment fragment = new SettingFragment();
        fragment.show(getFragmentManager(),"");
      }
    });
  }
}
