package com.felizcube.bleremote;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Random;

/**
 * Created by LutherHsieh on 2017/7/7.
 */

public class LUtil
{
  public static final int DIALOG_TEXT_COLOR = 0xFF00A29A;

  private static Runnable mYesProcedure;
  private static Runnable mNoProcedure;


  public static boolean isHexChar(char c)
  {
    return (( c>='0' && c <='9') ||
            ( c>='A' && c <='F') ||
            ( c>='a' && c <='f') );
  }

//  public static boolean isAlphaChar(char c)
//  {
//    return (( c>='0' && c <='9') ||
//      ( c>='A' && c <='Z') ||
//      ( c>='a' && c <='z') );
//  }


  public static byte[] getRandomPrintableBytes(int len)
  {
    byte[] res = null;
    if (len <= 0)
    {
      return res;
    }

    res = new byte[len];
    Random rnd = new Random();
    for (int i=0;i<len;i++)
    {
      while(true)
      {
        byte b = (byte)rnd.nextInt();
        if (isAsciiPrintable((char)b))
        {
          res[i] = b;
          break;
        }
      }
    }

    return res;
  }

  public static String getDot(int count)
  {
    String res = "";
    switch (count)
    {
      case 1:
        res = "...";
        break;
      case 2:
        res = "..";
        break;
      default:
        res = ".";
        break;
    }
    return res;
  }

  public static int getDecimalFromHex(char c)
  {
    int res = 0;

    if ( c>='0' && c <='9')
    {
      res = c - '0';
    }
    else if ( c>='A' && c <='F')
    {
      res = 10 + (c - 'A');
    }
    else if ( c>='a' && c <='f')
    {
      res = 10 + (c - 'a');
    }

    return res;
  }


  public static String dumpBytes(byte[] ary)
  {
    return dumpBytes(ary, true);
  }

  public static String dumpBytes(byte[] ary, boolean isAddSeparate)
  {
    StringBuilder sb = new StringBuilder();
    if (ary != null)
    {
      String fmt = "%02X";
      if (isAddSeparate)
      {
        fmt += ",";
      }
      for (byte b : ary)
      {
        sb.append(String.format(fmt, b));
      }

      if (isAddSeparate)
      {
        if (sb.length() > 0)
        {
      /*移除最後一個 ,*/
          sb.deleteCharAt(sb.length() - 1);
        }
      }
    }
    return sb.toString();
  }

  public static byte[] convertHexStringToBytes(String str)
  {
    byte[] res = null;
    if (str == null || str.isEmpty())
    {
      return res;
    }
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    char[] cs = str.toCharArray();
    int ix = 0;
    int val = 0;
    for (char c : cs)
    {
      if (isHexChar(c))
      {
        if (ix == 0)
        {
          val = getDecimalFromHex(c) * 16;
          ix = 1;
        }
        else if (ix == 1)
        {
          val += getDecimalFromHex(c);
          baos.write(val);
          ix = 0;
        }
      }
      else
      {
        if (ix != 0)
        {
          baos.write(val);
        }
        ix = 0;
      }
    }

    if (baos.size() > 0)
    {
      res = baos.toByteArray();
    }

    return res;
  }

  public static void showYesNoDialog(Context ctx, int titleID, int msgID, Runnable yesProcedure, Runnable noProcedure)
  {
    mYesProcedure = yesProcedure;
    mNoProcedure = noProcedure;
    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
    if (titleID > 0)
    {
      builder.setTitle(titleID);
    }
    if (msgID > 0)
    {
      builder.setMessage(msgID);
    }

    builder.setNegativeButton(R.string.no,new DialogInterface.OnClickListener()
    {
      @Override
      public void onClick(DialogInterface arg0, int arg1)
      {
        if (mNoProcedure != null)
        {
          mNoProcedure.run();
        }
      }
    });
    builder.setPositiveButton(R.string.yes,new DialogInterface.OnClickListener()
    {
      @Override
      public void onClick(DialogInterface arg0, int arg1)
      {
        if (mYesProcedure != null)
        {
          mYesProcedure.run();
        }
      }
    });

    final AlertDialog dlg = builder.create();
    dlg.setOnShowListener( new DialogInterface.OnShowListener()
    {
      @Override
      public void onShow(DialogInterface arg0)
      {
        dlg.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(DIALOG_TEXT_COLOR);
        dlg.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(DIALOG_TEXT_COLOR);
      }
    });

    dlg.show();
  }

  public static void showErrorDialog(Context ctx, int titleID, int msgID)
  {
    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
    if (titleID > 0)
    {
      builder.setTitle(titleID);
    }
    if (msgID > 0)
    {
      builder.setMessage(msgID);
    }
    builder.setIcon(android.R.drawable.ic_dialog_alert);

    builder.create().show();
  }

  public static void showInfoDialog(Context ctx, int titleID, String msg)
  {
    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
    if (titleID > 0)
    {
      builder.setTitle(titleID);
    }
    if (msg != null)
    {
      builder.setMessage(msg);
    }
    builder.setIcon(android.R.drawable.ic_dialog_info);

    builder.create().show();
  }

  public static void showViewDialog(Context ctx, int titleID, View v)
  {
    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
    if (titleID > 0)
    {
      builder.setTitle(titleID);
    }
    builder.setView(v);
    builder.create().show();
  }

  /**
   * 取得 Date 的 Tick, 會移除 millisecond
   * @param d
   * @return
   */
  public static long getDateTickWithoutMS(Date d)
  {
    long tick = d.getTime();
    tick = (tick / 1000) * 1000;
    return tick;
  }



  /**
   * <p>Checks if the string contains only ASCII printable characters.</p>
   *
   * <p><code>null</code> will return <code>false</code>.
   * An empty String ("") will return <code>true</code>.</p>
   *
   * <pre>
   * StringUtils.isAsciiPrintable(null)     = false
   * StringUtils.isAsciiPrintable("")       = true
   * StringUtils.isAsciiPrintable(" ")      = true
   * StringUtils.isAsciiPrintable("Ceki")   = true
   * StringUtils.isAsciiPrintable("ab2c")   = true
   * StringUtils.isAsciiPrintable("!ab-c~") = true
   * StringUtils.isAsciiPrintable("\u0020") = true
   * StringUtils.isAsciiPrintable("\u0021") = true
   * StringUtils.isAsciiPrintable("\u007e") = true
   * StringUtils.isAsciiPrintable("\u007f") = false
   * StringUtils.isAsciiPrintable("Ceki G\u00fclc\u00fc") = false
   * </pre>
   *
   * @param str the string to check, may be null
   * @return <code>true</code> if every character is in the range
   *  32 thru 126
   * @since 2.1
   */
  public static boolean isAsciiPrintable(String str)
  {
    if (str == null)
    {
      return false;
    }
    int sz = str.length();
    for (int i = 0; i < sz; i++)
    {
      if (isAsciiPrintable(str.charAt(i)) == false)
      {
        return false;
      }
    }
    return true;
  }

  /**
   * <p>Checks whether the character is ASCII 7 bit printable.</p>
   *
   * <pre>
   *   CharUtils.isAsciiPrintable('a')  = true
   *   CharUtils.isAsciiPrintable('A')  = true
   *   CharUtils.isAsciiPrintable('3')  = true
   *   CharUtils.isAsciiPrintable('-')  = true
   *   CharUtils.isAsciiPrintable('\n') = false
   *   CharUtils.isAsciiPrintable('&copy;') = false
   * </pre>
   *
   * @param ch  the character to check
   * @return true if between 32 and 126 inclusive
   */
  public static boolean isAsciiPrintable(char ch)
  {
    return ch >= 32 && ch < 127;
  }

  /**
   * 修正 BLE MAC address 的字串
   * 可以不輸入 冒號, 由這邊 自動補上
   *
   * @param mac
   * @return
   */
  public static String CorrectMacAddress(String mac)
  {
    String res = "";

    if (mac == null || mac.isEmpty())
    {
      return res;
    }

    if (mac.length() == 12 && mac.indexOf(':') < 0)
    {
      char[] s = mac.toCharArray();
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < 12; i++)
      {
        sb.append(s[i]);
        if ((i & 0x01) != 0 && i != 11)
        {
          /*每兩個字元加上 冒號, 最後一個 不要加*/
          sb.append(':');
        }
      }

      res = sb.toString();
    }
    else
    {
      /*不修正*/
      return mac;
    }
    return res;
  }


  /**
   * 檢查 字串是否為 空字串 或是 NULL
   * @param str
   * @return
   */
  public static boolean isStrNull(String str)
  {
    return (str == null || str.isEmpty() || str == "");
  }


}
