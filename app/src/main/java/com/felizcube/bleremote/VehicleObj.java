package com.felizcube.bleremote;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Created by LutherHsieh on 2017/7/7.
 */

public class VehicleObj
{
  private static final String TAG = BTUtil.class.getSimpleName();
  public static int AUTH_UNKNOWN = 0;
  public static int AUTH_PASS = 1;
  public static int AUTH_FAIL = 2;
  public static int AUTH_CHECKING = 3;

  private static Random gRand = new Random();
  public String devName = "";
  public String mac = "";
  public int batState = 0;
  public int batVoltage = 0;
  public int batLevel = 0;
  public int batResistor = 0;
  public int batCca = 0;
  public Date recordTime = null;
  public byte[] rawData = null;
  public boolean isChecked = false;
  public int rssi = 0;
  public int authState = AUTH_UNKNOWN;

  public long myID = 0;
  public long lastAccessTick = 0;

  private static byte[][] KEY_CODE = new byte[][]{
    {(byte)0x2A,(byte)0xA7,(byte)0x76,(byte)0x7F,(byte)0x89,(byte)0x58,(byte)0xB8,(byte)0xBA,(byte)0xCB,(byte)0xB2,(byte)0xB2,(byte)0x75,(byte)0xED,(byte)0x10,(byte)0xB5,(byte)0x8C},
    {(byte)0x6F,(byte)0xFE,(byte)0x0C,(byte)0xCD,(byte)0x68,(byte)0x0A,(byte)0xF6,(byte)0x95,(byte)0x01,(byte)0xE6,(byte)0x33,(byte)0xA6,(byte)0x3E,(byte)0xB7,(byte)0x04,(byte)0x35},
    {(byte)0x01,(byte)0x06,(byte)0x3B,(byte)0xF5,(byte)0x99,(byte)0x0B,(byte)0x98,(byte)0xB4,(byte)0xC4,(byte)0x2E,(byte)0xB7,(byte)0xAC,(byte)0x64,(byte)0x09,(byte)0x58,(byte)0x77},
    {(byte)0x49,(byte)0xE9,(byte)0x9A,(byte)0x4F,(byte)0xB3,(byte)0x82,(byte)0x97,(byte)0x20,(byte)0xB0,(byte)0xE0,(byte)0x7D,(byte)0xA2,(byte)0x24,(byte)0x12,(byte)0x7B,(byte)0x13},
    {(byte)0xAE,(byte)0x7C,(byte)0x73,(byte)0xE7,(byte)0xD5,(byte)0xBF,(byte)0xF3,(byte)0x64,(byte)0x65,(byte)0x58,(byte)0x09,(byte)0x3A,(byte)0x52,(byte)0x4A,(byte)0x30,(byte)0xF8},
    {(byte)0x8D,(byte)0xBC,(byte)0x5D,(byte)0x55,(byte)0x1D,(byte)0xB4,(byte)0x81,(byte)0x7F,(byte)0x6F,(byte)0x1B,(byte)0x44,(byte)0xB8,(byte)0x8D,(byte)0xF5,(byte)0x8F,(byte)0xB2},
    {(byte)0xB3,(byte)0xA3,(byte)0xFE,(byte)0xE8,(byte)0x95,(byte)0x71,(byte)0xA1,(byte)0x89,(byte)0xF5,(byte)0x35,(byte)0xE9,(byte)0x5A,(byte)0x79,(byte)0x5D,(byte)0xCD,(byte)0x66},
    {(byte)0xB0,(byte)0x85,(byte)0x15,(byte)0x68,(byte)0xB4,(byte)0x26,(byte)0xF6,(byte)0xF9,(byte)0xFA,(byte)0xF4,(byte)0x77,(byte)0xBA,(byte)0x7B,(byte)0x3B,(byte)0xD7,(byte)0x6F},
    {(byte)0xC9,(byte)0x57,(byte)0x86,(byte)0xAE,(byte)0x4C,(byte)0x48,(byte)0x62,(byte)0x1B,(byte)0x1A,(byte)0xD3,(byte)0x5F,(byte)0xE8,(byte)0xCA,(byte)0x26,(byte)0x06,(byte)0x11},
    {(byte)0x4D,(byte)0xDE,(byte)0x07,(byte)0x08,(byte)0xE0,(byte)0x27,(byte)0xFD,(byte)0x8D,(byte)0xAD,(byte)0x04,(byte)0xED,(byte)0xA1,(byte)0xF9,(byte)0xE7,(byte)0x0E,(byte)0xBA},
    {(byte)0x5E,(byte)0x79,(byte)0x91,(byte)0x22,(byte)0xD6,(byte)0x1F,(byte)0x19,(byte)0x67,(byte)0x22,(byte)0xF9,(byte)0x4D,(byte)0xE1,(byte)0xC6,(byte)0x0E,(byte)0x2F,(byte)0xD3},
    {(byte)0xEE,(byte)0x66,(byte)0x6D,(byte)0x9C,(byte)0x34,(byte)0xCE,(byte)0x8F,(byte)0xA2,(byte)0x59,(byte)0xF9,(byte)0x00,(byte)0x48,(byte)0x5B,(byte)0xC1,(byte)0x21,(byte)0x33},
    {(byte)0xC4,(byte)0xA5,(byte)0x89,(byte)0x50,(byte)0x95,(byte)0xE4,(byte)0x3B,(byte)0x2E,(byte)0xC4,(byte)0x9F,(byte)0xE5,(byte)0xE6,(byte)0xD9,(byte)0x1B,(byte)0xC6,(byte)0x9F},
    {(byte)0xD4,(byte)0x19,(byte)0xB2,(byte)0x93,(byte)0x7A,(byte)0x72,(byte)0x86,(byte)0x8F,(byte)0xEA,(byte)0xEF,(byte)0xC8,(byte)0xDC,(byte)0x1E,(byte)0xD1,(byte)0x0C,(byte)0x61},
    {(byte)0xD7,(byte)0x19,(byte)0x1D,(byte)0x21,(byte)0x84,(byte)0xDF,(byte)0x3E,(byte)0x0A,(byte)0x2B,(byte)0x3E,(byte)0xD7,(byte)0x94,(byte)0xE9,(byte)0xA8,(byte)0xC8,(byte)0x3D},
    {(byte)0x33,(byte)0x61,(byte)0x73,(byte)0x41,(byte)0x0E,(byte)0x8E,(byte)0xB9,(byte)0x52,(byte)0x1E,(byte)0xF9,(byte)0x65,(byte)0xA5,(byte)0x1B,(byte)0x5E,(byte)0x0F,(byte)0xF3}
  };

  /**
   * 解碼, 直接把 encData 的內容 解碼
   *
   * @param encData
   */
  public static void decode(byte[] encData)
  {
    /*扣除 第1, 2, 以及最後 1個 所以要大於 3 才能解碼 */
    if (encData == null || encData.length < 4)
    {
      return;
    }
    /*取出 解碼 Key*/
    if (encData[0] < 0 || encData[0] > 9)
    {
      return;
    }
    byte[] code = KEY_CODE[encData[0]];

    int len = encData.length - 3;
    if (len > code.length)
    {
      len = code.length;
    }

    for (int i = 0; i < len; i++)
    {
      encData[i + 2] ^= code[i];
    }
  }

  public static void encode(byte[] data)
  {
    /*扣除 第1, 2, 以及最後 1個 所以要大於 3 才能解碼 */
    if (data == null || data.length < 4)
    {
      return;
    }
    /*取出 解碼 Key*/
    if (data[0] < 0 || data[0] > 9)
    {
      return;
    }
    byte[] code = KEY_CODE[data[0]];

    int len = data.length - 2;
    if (len > code.length)
    {
      len = code.length;
    }

    for (int i = 0; i < len; i++)
    {
      data[i + 1] ^= code[i];
    }
  }

  public static byte getNumEncode(int val)
  {
    int big = (val / 10);
    return (byte) ((big * 16) + (val % 10));

  }

  public static int getNumDecode(byte encVal)
  {
    int res = 0;
    res = ((encVal >> 4) & 0x0F) * 10;
    res += (encVal & 0x0F);
    return res;
  }

  public static byte getRandomCodeIx()
  {
    return (byte)gRand.nextInt(10);
  }


  public static byte[] getCmdGhost()
  {
    byte[] res = new byte[1];
    res[0] = 0x03;
    //res[0] = 0x01; /*01 好像只會傳第一與最後一筆*/

    return res;
  }



  public static byte[] getCmdSetTime()
  {
    byte[] res = new byte[9];
    res[0] = getRandomCodeIx();
    res[1] = 0x01;
    Calendar c = Calendar.getInstance();
    c.setTime(new Date());
//    res[2] = getNumEncode((c.get(Calendar.YEAR) % 100));
//    res[3] = getNumEncode((c.get(Calendar.MONTH) +1));
//    res[4] = getNumEncode((c.get(Calendar.DAY_OF_MONTH)));
//    res[5] = getNumEncode((c.get(Calendar.HOUR_OF_DAY)));
//    res[6] = getNumEncode((c.get(Calendar.MINUTE)));
//    res[7] = getNumEncode((c.get(Calendar.SECOND)));

    res[2] = (byte)(c.get(Calendar.YEAR) % 100);
    res[3] = (byte)(c.get(Calendar.MONTH) +1);
    res[4] = (byte)(c.get(Calendar.DAY_OF_MONTH));
    res[5] = (byte)(c.get(Calendar.HOUR_OF_DAY));
    res[6] = (byte)(c.get(Calendar.MINUTE));
    res[7] = (byte)(c.get(Calendar.SECOND));

//    /*測試用*/
//    res[2] = (byte)7;
//    res[3] = (byte)7;
//    res[4] = (byte)9;
//    res[5] = (byte)45;
//    res[6] = (byte)55;
//    res[7] = (byte)45;



    res[8] = getCheckSum(res);
    //Log.d(TAG, "Set Time:"+LUtil.dumpBytes(res));
    VehicleObj.encode(res);
    return res;
  }

  public static byte[] getCmdTest()
  {
    byte[] res = new byte[9];
    res[0] = getRandomCodeIx();
    res[1] = 0x02;
    for (int i=2;i<8;i++)
    {
      res[i] = (byte)gRand.nextInt(256);
    }
    res[8] = getCheckSum(res);
    VehicleObj.encode(res);
    return res;
  }

  public static VehicleObj fromPacket(byte[] packet)
  {
    VehicleObj res = null;
    if (packet == null || packet.length < 19)
    {
      return res;
    }
    res = new VehicleObj();
    Calendar c = Calendar.getInstance();
    int y = getNumDecode(packet[2]) + 2000;
    int m = getNumDecode(packet[3]) -1;
    int d = getNumDecode(packet[4]);
    int h = getNumDecode(packet[5]);
    int min = getNumDecode(packet[6]);
    int s = getNumDecode(packet[7]);
    c.set(y,m,d,h, min, s);
    res.recordTime = c.getTime();
    res.batState = getNumDecode(packet[8]);
    int tmp = 0;
    tmp = getNumDecode(packet[9]) * 100;
    res.batVoltage = tmp + getNumDecode(packet[10]);
    tmp = getNumDecode(packet[11]) * 100;
    res.batLevel = tmp + getNumDecode(packet[12]);
    int plus = 2 - getNumDecode(packet[13]);
    if (plus < 0)
    {
      plus = 0;
    }
    plus = (int)Math.pow(10, plus);
    tmp = getNumDecode(packet[14]) * 100;
    res.batResistor = (tmp + getNumDecode(packet[15])) * plus;
    tmp = getNumDecode(packet[16]) * 100;
    res.batCca = tmp + getNumDecode(packet[17]);

    return res;
  }



  /**
   * 檢查 封包是否 Checksum 正確?
   * @param packet
   * @return
   */
  public static boolean isValidate(byte[] packet)
  {
    boolean res = false;
    /*扣除 第1, 2, 以及最後 1個 所以要大於 3 才能解碼 */
    if (packet == null || packet.length < 4)
    {
      return res;
    }
    res = (packet[packet.length - 1] == getCheckSum(packet));

    return res;
  }

  public static byte getCheckSum(byte[] packet)
  {
    byte res = 0;
    /*扣除 第1, 2, 以及最後 1個 所以要大於 3 才能解碼 */
    if (packet == null || packet.length < 4)
    {
      return res;
    }
    int len = packet.length - 1;
    for (int i=0;i<len;i++)
    {
      res += packet[i];
    }

    return res;
  }




  public VehicleObj()
  {

  }

  private DataObj mDataObj = DataObj.getInstance();

  public String getDisplayName()
  {
    return mDataObj.getVehicleDisplayName(this);
  }



  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("Name:"+devName);
    sb.append(",Mac:"+mac);
    sb.append(",batState:"+batState);
    sb.append(",batVoltage:"+batVoltage);
    sb.append(",batLevel:"+batLevel);
    sb.append(",batResistor:"+batResistor);
    sb.append(",batCca:"+batCca);
    if (recordTime != null)
    {
      sb.append(",recordTime:"+recordTime.toString());
    }

    return sb.toString();
  }


  public void updateRssi(int rssi)
  {
    this.rssi = rssi;
    lastAccessTick = System.currentTimeMillis();
  }

  /**
   * check the device is live?
   *
   * @return
   */
  public boolean isLive()
  {
    /* get the beacon inside 60 seconds */
    long tick = System.currentTimeMillis();
    if (tick < this.lastAccessTick)
    {
      return false;
    }
    else
    {
      return ((tick - this.lastAccessTick) <= 60000);
    }
  }



}
