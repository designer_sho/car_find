package com.felizcube.bleremote;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.felizcube.bleremote.DataRestore.CarData;
import com.felizcube.bleremote.DataRestore.SVCData;
import com.felizcube.bleremote.DataRestore.UIData;
import com.felizcube.bleremote.Fragment.PasswordFragment;
import com.felizcube.bleremote.Fragment.ProfileFragment;
import com.felizcube.bleremote.Fragment.SettingFragment;

import java.util.ArrayList;


public class Main extends Activity implements BTVehicleHandler
{
  private static final String TAG = Main.class.getSimpleName();
  private static final int REQUEST_AUTH_LOCATION = 0x2001;
  private ViewPager viewPager;
  private ArrayList<PageView> pageList;
  private RadioButton radioButton1;
  private RadioButton radioButton2;
  private ArrayList<ProgressBar> progressBars = new ArrayList();
  private static ArrayList<ImageView> imageViews = new ArrayList();
  private static ArrayList<TextView> textViews = new ArrayList();
  private LinearLayout carsbutton;
  private LinearLayout settingbutton;
  public static DisplayMetrics metrics = new DisplayMetrics();
  public static UIData data ;
  public static CarData car_data;
  public static SVCData svc_data;
  public static boolean password_lock;
  private DataObj mDataObj;
  private BTUtil mBTUtil;

  private Handler mUIHandler = new Handler(Looper.getMainLooper())
  {
    @Override
    public void handleMessage(Message message)
    {
      // This is where you do your work in the UI thread.
      // Your worker tells you in the message what to do.
      //if (message.what == DataObj.MSG_STOP_ADVERTISE)
    }
  };

  private View mDecorView = null;

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    /*讓畫面全螢幕, 包含 Home Key 在螢幕上的手機*/
    if (hasFocus) {
      mDecorView.setSystemUiVisibility(
          View.SYSTEM_UI_FLAG_LAYOUT_STABLE
           | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
          | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
          | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
          | View.SYSTEM_UI_FLAG_FULLSCREEN
          | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
  }

  @Override protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    //Remove Notification Bar (程式名稱)
    requestWindowFeature(Window.FEATURE_NO_TITLE);

    //Remove Title Bar (電池, 3G, Wifi 通知)
    //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
    //  WindowManager.LayoutParams.FLAG_FULLSCREEN);

    setContentView(R.layout.main);
    // 請注意, 如果上面那兩段 Remove Code 擺到 setContentView 下面的話會程式閃退

    mDecorView = getWindow().getDecorView();

    /*2017-07-14 發現 onCreate 在第一次安裝時 有機會 被重複呼叫*/
    if (!isTaskRoot())
    {
      Log.d(TAG, "Not TaskRoot close it.");
      finish();
      return;
    }

    InitData();
    InitUI();
    InitUIFunction();

    /* 啟動 藍芽 */
    mBTUtil = new BTUtil(Main.this, Main.this, mUIHandler);

  }

  private void InitData()
  {
    mDataObj = DataObj.getInstance();
    data = new UIData(getApplicationContext());
    car_data = new CarData(getApplicationContext());
    svc_data = new SVCData(getApplicationContext());
  }

  private void InitUI()
  {
    viewPager = (ViewPager)findViewById(R.id.viewpager);
    radioButton1 = (RadioButton)findViewById(R.id.radioButton1);
    radioButton2 = (RadioButton)findViewById(R.id.radioButton2);
    pageList = new ArrayList<>();
    pageList.add(new PageOneView(Main.this));
    pageList.add(new PageTwoView(Main.this));
    viewPager.setAdapter(new myPagerAdapter());
    carsbutton = (LinearLayout)findViewById(R.id.carsbutton);
    settingbutton = (LinearLayout)findViewById(R.id.settingbutton);
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    if(data.enable_password && data.code.length()!=0)
    {
      password_lock = true;
      PasswordFragment fragment = new PasswordFragment();
      fragment.show(getFragmentManager(),"");
    }
  }

  private void InitUIFunction()
  {
    /* create circle indicator*/
    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
    {
      @Override
      public void onPageScrolled(int position,float positionOffset,int positionOffsetPixels)
      {

      }

      @Override public void onPageSelected(int position)
      {
        switch(position)
        {
          case 0:
            radioButton1.setChecked(true);
            break;
          case 1:
            radioButton2.setChecked(true);
            break;
        }
      }

      @Override public void onPageScrollStateChanged(int state)
      {

      }
    });
    carsbutton.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        ProfileFragment fragment = new ProfileFragment();
        fragment.show(getFragmentManager(),"");
      }
    });
    settingbutton.setOnClickListener(new View.OnClickListener()
    {
      @Override public void onClick(View v)
      {
        SettingFragment fragment = new SettingFragment();
        fragment.show(getFragmentManager(),"");
      }
    });
  }

  public static void UpdateData()
  {
    for(int i = 0 ; i < textViews.size() ; i++)
    {
      textViews.get(i).setText(data.priority.get(i).toString());
    }
  }

  /*========================================*/
  /* Implements BTVehicleHandler ---START---*/
  /*========================================*/
  private String mLastGotMac = "";
  private boolean mIsConnected = false;
  @Override
  public void addResult(ScanResult result)
  {
    mLastGotMac = result.getDevice().getAddress();
    Main.this.runOnUiThread(new Runnable()
    {
      public void run()
      {
        Toast.makeText(Main.this, "Found BLE Device.", Toast.LENGTH_SHORT).show();
      }
    });

  }

  @Override
  public void saveVehicleToDB(VehicleObj vObj)
  {

  }

  @Override
  public void updateVehicleView(VehicleObj vObj)
  {

  }

  @Override
  public boolean isBTHanlderBusy()
  {
    return false;
  }

  @Override
  public void notifyAuthedVehicle(VehicleObj vObj)
  {

  }

  @Override
  public void notifyCommandTimeout()
  {

  }

  @Override
  public void notifyConnected()
  {
    mIsConnected = true;

    Log.d(TAG, "Connected!");
    Main.this.runOnUiThread(new Runnable()
    {
      public void run()
      {
        Toast.makeText(Main.this, "Connected", Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public void notifyBTError()
  {

  }

  /*========================================*/
  /* Implements BTVehicleHandler --- END ---*/
  /*========================================*/


  /*adapter*/
  private class myPagerAdapter extends PagerAdapter
  {
    @Override public int getCount()
    {
      return pageList.size();
    }

    @Override public boolean isViewFromObject(View view,Object o)
    {
      return o == view;
    }

    @Override public Object instantiateItem(ViewGroup container,int position)
    {
      container.addView(pageList.get(position));
      return pageList.get(position);
    }

    @Override public void destroyItem(ViewGroup container,int position,Object object)
    {
      container.removeView((View)object);
    }
  }

  public class PageView extends LinearLayout
  {
    public PageView(Context context)
    {
      super(context);
    }
  }

  public class PageOneView extends PageView
  {
    public PageOneView(Context context)
    {
      super(context);
      View view = LayoutInflater.from(context).inflate(R.layout.page_content,null);
      ImageView iv1 = (ImageView)view.findViewById(R.id.iv1);
      ImageView iv2 = (ImageView)view.findViewById(R.id.iv2);
      ImageView iv3 = (ImageView)view.findViewById(R.id.iv3);
      iv1.setOnClickListener(new OnClickListener()
      {
        @Override public void onClick(View v)
        {
          AddClick(0);
        }
      });
      iv2.setOnClickListener(new OnClickListener()
      {
        @Override public void onClick(View v)
        {
          AddClick(1);
        }
      });
      iv3.setOnClickListener(new OnClickListener()
      {
        @Override public void onClick(View v)
        {
          AddClick(2);
        }
      });
      imageViews.add(iv1);
      imageViews.add(iv2);
      imageViews.add(iv3);
      TextView tv1 = (TextView)view.findViewById(R.id.tv1);
      TextView tv2 = (TextView)view.findViewById(R.id.tv2);
      TextView tv3 = (TextView)view.findViewById(R.id.tv3);
      tv1.setText(data.priority.get(0).toString());
      tv2.setText(data.priority.get(1).toString());
      tv3.setText(data.priority.get(2).toString());
      textViews.add(tv1);
      textViews.add(tv2);
      textViews.add(tv3);
      progressBars.add((ProgressBar)view.findViewById(R.id.pb0));
      progressBars.add((ProgressBar)view.findViewById(R.id.pb1));
      progressBars.add((ProgressBar)view.findViewById(R.id.pb2));
      addView(view);
    }
  }

  public class PageTwoView extends PageView
  {
    public PageTwoView(Context context)
    {
      super(context);
      View view = LayoutInflater.from(context).inflate(R.layout.page_content,null);
      ImageView iv1 = (ImageView)view.findViewById(R.id.iv1);
      ImageView iv2 = (ImageView)view.findViewById(R.id.iv2);
      ImageView iv3 = (ImageView)view.findViewById(R.id.iv3);
      iv1.setOnClickListener(new OnClickListener()
      {
        @Override public void onClick(View v)
        {
          AddClick(3);
        }
      });
      iv2.setOnClickListener(new OnClickListener()
      {
        @Override public void onClick(View v)
        {
          AddClick(4);
        }
      });
      iv3.setOnClickListener(new OnClickListener()
      {
        @Override public void onClick(View v)
        {
          AddClick(5);
        }
      });
      imageViews.add(iv1);
      imageViews.add(iv2);
      imageViews.add(iv3);
      TextView tv1 = (TextView)view.findViewById(R.id.tv1);
      TextView tv2 = (TextView)view.findViewById(R.id.tv2);
      TextView tv3 = (TextView)view.findViewById(R.id.tv3);
      tv1.setText(data.priority.get(3).toString());
      tv2.setText(data.priority.get(4).toString());
      tv3.setText(data.priority.get(5).toString());
      textViews.add(tv1);
      textViews.add(tv2);
      textViews.add(tv3);
      progressBars.add((ProgressBar)view.findViewById(R.id.pb0));
      progressBars.add((ProgressBar)view.findViewById(R.id.pb1));
      progressBars.add((ProgressBar)view.findViewById(R.id.pb2));
      addView(view);
    }
  }

  private void AddClick(final int index)
  {
    imageViews.get(index).setVisibility(View.INVISIBLE);
    progressBars.get(index).setVisibility(View.VISIBLE);
    switch(data.priority.get(index).toString())
    {
      case "unlock":
      {
        UnLock();
        break;
      }
      case "start":
      {
        Start();
        break;
      }
      case "lock":
      {
        Lock();
        break;
      }
      case "aux1":
      {
        AUX1();
        break;
      }
      case "panic":
      {
        Panic();
        break;
      }
      case "trunk":
      {
        Trunk();
        break;
      }
    }
    /*test*/
    new CountDownTimer(3000,1000)
    {
      @Override public void onFinish()
      {
        imageViews.get(index).setVisibility(View.VISIBLE);
        progressBars.get(index).setVisibility(View.INVISIBLE);
      }

      @Override public void onTick(long millisUntilFinished)
      {

      }
    }.start();
  }

  /* Debug, 測試用*/
  private boolean mIsScanning = false;
  public void onClick(View v) {

    int id = v.getId();
    switch (id){
      case R.id.mBtnTest1:
        /*開始 scan*/
        mIsScanning = !mIsScanning;
        if (mIsScanning)
        {
          /*沒有取得權限, 則離開*/
          if (!mBTUtil.checkBLEEnable() || !grantPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_AUTH_LOCATION))
          {
            return;
          }
        }
        mBTUtil.scanLeDevice(mIsScanning);
        Toast.makeText(this, (mIsScanning) ? "Start Scan.":"Stop Scan.", Toast.LENGTH_SHORT).show();
        break;
      case R.id.mBtnTest2:
        if (LUtil.isStrNull(mLastGotMac))
        {
          Toast.makeText(this, "No device found.", Toast.LENGTH_SHORT).show();
        }
        else
        {
          VehicleObj vObj = mDataObj.getVehicleByMacInExistList(mLastGotMac);
          if (vObj == null)
          {
            Toast.makeText(this, "No device found2.", Toast.LENGTH_SHORT).show();
          }
          else
          {
            mBTUtil.doConnect(vObj);
            Toast.makeText(this, "Connect to the device.", Toast.LENGTH_SHORT).show();
          }
        }
        break;
    }
  }

  private boolean grantPermission(String strPermission, int authCode)
  {
    boolean res = true;
    int tmpPerm = 0;
    if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(this, strPermission))
    {
      res = false;
      ActivityCompat.requestPermissions(Main.this, new String[]{strPermission}, authCode);
    }

    return res;
  }

  /*button Listener*/
  private void UnLock()
  {
    Tool.LOG("unlock");
  }

  private void Start()
  {
    Tool.LOG("start");
    /*測試 發送資料*/
    if (mIsConnected)
    {
      mBTUtil.sendData("Start".getBytes());
    }
  }

  private void Lock()
  {
    Tool.LOG("lock");
  }

  private void AUX1()
  {
    Tool.LOG("AUX1");
  }

  private void Panic()
  {
    Tool.LOG("panic");
  }

  private void Trunk()
  {
    Tool.LOG("trunk");
  }



  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
  {
    switch (requestCode)
    {
      case REQUEST_AUTH_LOCATION:
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
          //取得權限
//          /*開啟掃描*/
//          Log.d(TAG, "Enable scanLeDevice!");
//          mBTUtil.scanLeDevice(true);
        }
        else
        {
          //使用者拒絕權限， 關閉程式囉!
          finish();
        }
        break;
    }
  }



}
