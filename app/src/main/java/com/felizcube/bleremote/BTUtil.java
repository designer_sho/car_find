package com.felizcube.bleremote;

/**
 * Created by LutherHsieh on 2017/7/7.
 */

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

/**
 * 處理 藍芽相關的 小程式,
 */
public class BTUtil
{
  private static final String TAG = BTUtil.class.getSimpleName();

  private static final int REQUEST_ENABLE_BT = 1;

  public static final int RESULT_UNKNOW = 0;
  public static final int RESULT_OK = 1;
  public static final int RESULT_FAIL = 2;
  public static final int RESULT_FAIL_AND_STOP = 3;
  public static final int RESULT_TIMEOUT = 4;

  public static final int CONN_NO_CONNECT = 0; /*未連線*/
  public static final int CONN_CONNECTED = 1;
  public static final int CONN_DISCONNECT = 2;
  public static final int CONN_TRY_CONNECT = 3;
  public static final int CONN_SEND_CMD = 4;
  public static final int CONN_WAIT_REPORT = 5;
  public static final int CONN_CMD_TIMEOUT = 6;

  //private int cmdResultCode = RESULT_UNKNOW;
  //private int connectionState = CONN_NO_CONNECT;

  private BluetoothAdapter mBTAdapter;
  private BluetoothLeScanner mBTScanner;
  private BluetoothLeAdvertiser mBTAdvertiser;
  private Activity mActivity;
  private List<ScanResult> mScanResult;
  private ScanCallback mScanCallback;
  private Handler mHandler;
  private List<ScanFilter> mBTFilters;
  private ScanSettings mBTSettings;
  private boolean mIsScanning = false;
  private BTVehicleHandler mBTVehicleHandler;
  private AdvertiseSettings mADSettings;
  private AdvertiseCallback mAdvertiseCallback;
  private DataObj mDataObj = DataObj.getInstance();
  private Handler mUIHandler;
  private VehicleObj mConnectingBatteryObj = null;
//  private Handler mCheckConnectTimeoutHandler = new Handler();
//  private Handler mWaitJobFinishHandler = new Handler();
  private Hashtable<String, MyBTGattCB> mGattCBMap = new Hashtable<>();

  public boolean mIsBatteryScanning = false;

  public static final int JOB_RECONNECT = 1;
  public static final int JOB_BATTERY_TEST = 2;
  public static final int JOB_SCAN = 3;
  public static final int JOB_SHUTDOWN = 4;

  private int mWaitConnectResult = RESULT_UNKNOW;
  //public boolean mIsJobRunning = false;
  //public int mJobID = 0;

  public static final ParcelUuid BT_VEHICLE_AUTH_SERVICE_UUID = ParcelUuid
    .fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");

  public static final ParcelUuid BT_VEHICLE_READ_CHARACTER_UUID = ParcelUuid
    .fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");

  public static final ParcelUuid BT_VEHICLE_WRITE_CHARACTER_UUID = ParcelUuid
    .fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");

  public static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");


  public int getConnectionState()
  {
    int res = CONN_NO_CONNECT;
    if (mGattCB != null)
    {
      //Log.d(TAG, String.format("getConnectionState %d, %s",mGattCB.mConnectionState, mGattCB.mac) );
      res = mGattCB.mConnectionState;
    }
    return res;
  }

  /*連線不會立即完成, 由此 flag 來判斷*/
  public int getWaitConnectResult()
  {
    return mWaitConnectResult;
  }

  public boolean isConnected()
  {
    boolean res = false;
    if (mGattCB != null)
    {
      res = mGattCB.isConnected();
    }
    return res;
  }

  public BTUtil(Activity act, BTVehicleHandler h, Handler uiHand)
  {
    mActivity = act;
    mBTVehicleHandler = h;
    mUIHandler = uiHand;

    /* 檢查藍牙是否開啟 */
    if (checkBLEEnable())
    {
      InitBT();
    }


  }

  public boolean checkBLEEnable()
  {
    boolean res = false;

    // Use this check to determine whether BLE is supported on the device.
    // Then you can
    // selectively disable BLE-related features.
    if (!mActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE))
    {
      Toast.makeText(mActivity, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
      mActivity.finish();
    }

    mBTAdapter = BluetoothAdapter.getDefaultAdapter();
    if (mBTAdapter == null)
    {
      Toast.makeText(mActivity, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
      mActivity.finish();
    }
    /* 檢查藍牙是否開啟 */
    if (!mBTAdapter.isEnabled())
    {
      Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
      mActivity.startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
      /*確認有啟動之後 才會初始化*/
    }
    else
    {
      res = true;
    }
    return res;
  }



  public void HandleActivityResult(int requestCode, int resultCode, Intent data)
  {
    if (requestCode == REQUEST_ENABLE_BT)
    {
      if (resultCode != 0)
      {
        InitBT();
        //if (mIsBatteryScanning)
        {
          /*開啟掃描*/
          scanLeDevice(true);
        }
      }
      else
      {
        /*不啟動 藍芽 就 關閉程式*/
        if (mActivity != null)
        {
          mActivity.finish();
        }
      }
    }
  }

  /**
   * Returns an AdvertiseSettings object set to use low power (to help
   * preserve battery life) and disable the built-in timeout since this code
   * uses its own timeout runnable.
   */
  private AdvertiseSettings buildAdvertiseSettings()
  {
    AdvertiseSettings.Builder settingsBuilder = new AdvertiseSettings.Builder();

    settingsBuilder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER);
    settingsBuilder.setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH);
    settingsBuilder.setTimeout(0);
    return settingsBuilder.build();
  }

  /**
   * Custom callback after Advertising succeeds or fails to start. Broadcasts
   * the error code in an Intent to be picked up by AdvertiserFragment and
   * stops this Service.
   */
  private class SampleAdvertiseCallback extends AdvertiseCallback
  {

    @Override
    public void onStartFailure(int errorCode)
    {
      super.onStartFailure(errorCode);

      Log.d(TAG, "Advertising failed:" + errorCode);
    }

    @Override
    public void onStartSuccess(AdvertiseSettings settingsInEffect)
    {
      super.onStartSuccess(settingsInEffect);
      Log.d(TAG, "Advertising successfully started");
    }
  }

  private void InitBT()
  {
    mBTScanner = mBTAdapter.getBluetoothLeScanner();
    mBTAdvertiser = mBTAdapter.getBluetoothLeAdvertiser();
    mScanResult = new ArrayList<ScanResult>();
    mHandler = new Handler();


    mBTSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
    mBTFilters = new ArrayList<ScanFilter>();

    mADSettings = buildAdvertiseSettings();
    mAdvertiseCallback = new SampleAdvertiseCallback();

    if (Build.VERSION.SDK_INT >= 21)
    {
      if (mScanCallback != null)
      {
        mBTScanner.stopScan(mScanCallback);
      }
      mScanCallback = new ScanCallback()
      {
        @Override
        public void onScanResult(int callbackType, ScanResult result)
        {
          //Log.i(TAG, String.format("ScanResult: Type=%d, result=%s", callbackType, result));
          Log.i(TAG, String.format("ScanResult: Type=%d", callbackType));

          /* 2017-07-09 找到裝置, 應該要先確認, 是否為 可連線的裝置, 才顯示出來 */
          VehicleObj vehicleObj = BTUtil.decodeBLEScanResultToBatteryObj(result);
          if (vehicleObj != null)
          {
            /*檢查是否有存在?*/
            VehicleObj origVehicle = mDataObj.getVehicleByMacInExistList(vehicleObj.mac);
            if (origVehicle == null)
            {
              /*加入*/
              Log.i(TAG, String.format("ScanResult Add new VehicleObj. " + vehicleObj.mac));
              mDataObj.addVehicle(vehicleObj);
              origVehicle = vehicleObj;
              /* 確認有 新增 才會通知 */
              if (mBTVehicleHandler != null)
              {
                mBTVehicleHandler.addResult(result);
              }
            }
            if (origVehicle.authState == VehicleObj.AUTH_UNKNOWN ||
              origVehicle.authState == VehicleObj.AUTH_CHECKING)
            {
              Log.i(TAG, String.format("ScanResult do connection confirm. " + vehicleObj.mac));
              /*需要確認中*/
              //connect(origBat);
              //doReconnectDevice(origBat);
              /*必須在 掃描模式, 才會進行確認*/
//              if (mIsBatteryScanning)
//              {
//                mDataObj.addJobCmd(BleCmdFactory.makeBatteryAuthCmd(origBat));
//              }
              /*2017-07-12 改為不掃描.*/
              /* 把它加入 已認證清單*/
              origVehicle.authState = VehicleObj.AUTH_PASS;
              mDataObj.addAuthedVehicle(origVehicle);
              mDataObj.addVehicle(origVehicle);
              if (mBTVehicleHandler != null)
              {
                mBTVehicleHandler.notifyAuthedVehicle(origVehicle);
              }
              //Log.d(TAG, "Battery Auth OK. add to list.");

            }
            /*關閉 自動連線功能, 看看會不會比較穩定*/
//            /*2017-07-11 若是已認證, 而且 未連線, 則自動連線,
//            * 呵呵 這邊要避免正在 Scan 的情況*/
//            else if (!mIsBatteryScanning && origBat.authState == BatteryObj.AUTH_PASS)
//            {
//              /*必須是 已連線物件*/
//              BatteryObj connObj = mDataObj.getLinkedBattery();
//              if (connObj != null && connObj.mac.equals(origBat.mac))
//              {
//                /*如果沒有連線, 則自動再連線*/
//                int state = getConnectionState();
//                if (state == CONN_DISCONNECT || state == CONN_NO_CONNECT)
//                {
//                  /*自動連線*/
//                  //connect(origBat);
//                  //doReconnectDevice(origBat);
//                  mDataObj.addJobCmd(BleCmdFactory.makeBatteryConnectCmd(origBat));
//                }
//              }
//            }

          }


        }

        @Override
        public void onBatchScanResults(List<ScanResult> results)
        {
          for (ScanResult sr : results)
          {
            Log.i("TAG", String.format("Batch ScanResult: Results=%s", sr.toString()));
          }
        }

        @Override
        public void onScanFailed(int errorCode)
        {
          Log.e("TAG", String.format("Scan Failed: ErrCode=%d", errorCode));
          //clsDialog.Show(DeviceScanActivity.this, Application.strTitle,
          //  String.format("Scan Failed, Error Code = %d", errorCode));
          if (mBTVehicleHandler != null && errorCode != SCAN_FAILED_ALREADY_STARTED)
          {
            mBTVehicleHandler.notifyBTError();
          }
        }
      };
    }
  }




  public static VehicleObj decodeBLEScanResultToBatteryObj(ScanResult result)
  {
    if (result == null)
    {
      return null;
    }

    final ScanRecord scanRecord = result.getScanRecord();
    final BluetoothDevice btDevice = result.getDevice();
    //final byte[] rawData = scanRecord.getBytes();

    VehicleObj vehicleObj = new VehicleObj();
    vehicleObj.rssi = result.getRssi();
    vehicleObj.mac = btDevice.getAddress();
    vehicleObj.devName = btDevice.getName();
    vehicleObj.rawData = scanRecord.getBytes();

    return vehicleObj;
  }


  /**
   * 掃描 或是 停止
   *
   * @param enable
   */
  public void scanLeDevice(final boolean enable)
  {

    if (enable)
    {
      try
      {
        mBTScanner.startScan(mBTFilters, mBTSettings, mScanCallback);
        Log.d(TAG, "BLE Scan Enable!");
        mIsScanning = true;
      }
      catch (Exception ex)
      {
        Log.d(TAG, String.format("scanLeDevice err:%s", ex.toString()));
        //clsDialog.Show(DeviceScanActivity.this, "Error", "Fail to start scan ble device!\n" + ex.getMessage());
      }
//      if (!mIsScanning)
//      {
//
//      }
    }
    else
    {
      try
      {
        mBTScanner.stopScan(mScanCallback);
        Log.d(TAG, "BLE Scan Disable!");
      }
      catch (Exception ex)
      {
        Log.d(TAG, String.format("scanLeDevice err:%s", ex.toString()));
        //clsDialog.Show(DeviceScanActivity.this, "Error", "Fail to start scan ble device!\n" + ex.getMessage());
      }
//      if (mIsScanning)
//      {
//
//        mIsScanning = false;
//      }
    }
  }


  //private BluetoothGatt mGatt;
  private Runnable mJobCheckConnectTimeoutRunnable = null;

  private boolean doConnectNow(MyBTGattCB gattCB, BluetoothDevice device, VehicleObj vehicleObj)
  {
    boolean res = false;
    try
    {
    /*進入檢查狀態*/
//      mGattCB = new MyBTGattCB();
//      mGattCB.mac = vehicleObj.mac;
      mConnectingBatteryObj = vehicleObj;

    /*刪除確認的功能*/
//    if (isCheckAuthorization)
//    {
//      vehicleObj.authState = BatteryObj.AUTH_CHECKING;
//    }
//    else
      {
      /*標記為  檢查 是否為 Battery 裝置*/
        gattCB.mConnectionState = CONN_TRY_CONNECT;
      }

//    if (vehicleObj.authState == BatteryObj.AUTH_UNKNOWN)
//    {
//      vehicleObj.authState = BatteryObj.AUTH_CHECKING;
//    }
//    else
//    {
//      /*標記為  檢查 是否為 Battery 裝置*/
//      mGattCB.mConnectionState = CONN_TRY_CONNECT;
//    }

      mBTGatt = device.connectGatt(mActivity.getApplicationContext(), false, gattCB);
//    /* 5秒後 檢查是否 未連線?*/
//    mJobCheckConnectTimeoutRunnable = new Runnable(){
//      @Override
//      public void run() {
//        if (mGattCB != null && mGattCB.mConnectionState == CONN_TRY_CONNECT && btGatt != null)
//        {
//          /* Timeout 連線未連建, 強制離線.*/
//          try
//          {
//            btGatt.disconnect();
//          }catch(Exception e){};
//
//          mGattCB.mConnectionState = CONN_NO_CONNECT;
//          mGattCB.mCmdResultCode = RESULT_TIMEOUT;
//        }
//
//      }};
//    mCheckConnectTimeoutHandler.postDelayed(mJobCheckConnectTimeoutRunnable, 5000);

      if (gattCB != null)
      {
        Log.d(TAG, String.format("Trying to create a new connection. %s", gattCB.mac));
      }
      res = true;
    }
    catch(Exception e)
    {
    }
    return res;
  }

  /*刪除確認的功能*/
  //public boolean doConnect(BatteryObj vehicleObj, boolean isCheckAuthorization)
  public boolean doConnect(VehicleObj vehicleObj)
  {
    boolean isDelay = false;
    final VehicleObj myVehicleObj = vehicleObj;
    final BluetoothDevice device = mBTAdapter.getRemoteDevice(vehicleObj.mac);
    mWaitConnectResult = RESULT_UNKNOW;
    if (device == null)
    {
      Log.w(TAG, "Device not found.  Unable to connect.");
      return false;
    }
    //Log.d(TAG, String.format("doConnect bound:%d", device.getBondState()));

    /*讓每一個 mGattCB 對應一個 mac*/
    mGattCB = mGattCBMap.get(vehicleObj.mac);
    if (mGattCB == null)
    {
      mGattCB = new MyBTGattCB();
      mGattCB.mac = vehicleObj.mac;
      mGattCBMap.put(vehicleObj.mac, mGattCB);
    }
    else
    {
      Log.d(TAG, String.format("doConnect mGattCB code:%d", mGattCB.mCmdResultCode));
      /*理論上 如果停止時 或工作結束 就會設定 mCmdResultCode */
      if (mGattCB.isShutdownFinish == false)
      {
        if (mGattCB.mCmdResultCode != RESULT_UNKNOW)
        {
          isDelay = true;
          doShutdownGatt();
        }
        else
        {
      /*busy*/
          return false;
        }
      }
    }

    if (isDelay)
    {
      mUIHandler.postDelayed(new Runnable()
      {
        @Override
        public void run()
        {
          if (doConnectNow(mGattCB, device, myVehicleObj))
          {
            mWaitConnectResult = RESULT_OK;
          }
          else
          {
            mWaitConnectResult = RESULT_FAIL;
          }
        }
      }, 500);
    }
    else
    {
      if (doConnectNow(mGattCB, device, vehicleObj))
      {
        mWaitConnectResult = RESULT_OK;
      }
      else
      {
        mWaitConnectResult = RESULT_FAIL;
      }
    }

    return true;
  }

  public void handlerConnectionFail()
  {
    /* Timeout 連線未連建, 強制離線.*/
    if (mBTGatt != null)
    {
      try
      {
        mBTGatt.disconnect();
      }
      catch (Exception e)
      {
      };
    }

    if (mGattCB != null)
    {
      mGattCB.mConnectionState = CONN_NO_CONNECT;
      mGattCB.mCmdResultCode = RESULT_TIMEOUT;
    }
  }


  private MyBTGattCB mGattCB = null;
  private BluetoothGatt mBTGatt = null;

  public boolean doShutdownGatt()
  {
    boolean res = false;
    if (mGattCB != null)
    {
      mGattCB.stopConnection();
      //mGattCB = null;
      res = true;
    }
    return res;
  }

  public boolean doBatteryTest(boolean isOnlySetTime)
  {
    boolean res = false;
    int state = getConnectionState();
    if ((mGattCB != null) &&
          (state == CONN_CONNECTED || state == CONN_CMD_TIMEOUT ))
    {
      mGattCB.DoBatteryTest(isOnlySetTime);
      res = true;
    }

    return res;
  }

//  public boolean doGhost()
//  {
//    boolean res = false;
//    int state = getConnectionState();
//    if ((mGattCB != null) &&
//      (state == CONN_CONNECTED || state == CONN_CMD_TIMEOUT ))
//    {
//      mGattCB.doGhost();
//      res = true;
//    }
//
//    return res;
//  }



  public void handlerBatteryTestTimeout()
  {
    if (mGattCB != null)
    {
      /*2017-07-11 將逾時的狀態設為  CONN_CMD_TIMEOUT*/
      mGattCB.mConnectionState = CONN_CMD_TIMEOUT;
      /*通知 UI 恢復, 顯示最後一筆*/
      if (mBTVehicleHandler != null)
      {
        mBTVehicleHandler.notifyCommandTimeout();
      }
    }
  }

  public void sendData(byte[] data)
  {
    if (mGattCB != null)
    {
      mGattCB.sendData(data);
    }
  }




  private class MyBTGattCB extends BluetoothGattCallback
  {
    public int mConnectionState = CONN_NO_CONNECT;
    public int mCmdResultCode = RESULT_UNKNOW;
    private BluetoothGatt myGatt = null;
    private int mJobBatteryTestStep = 0;
    private boolean mIsOnlySetTime = false;
    private BluetoothGattCharacteristic mWriteCharacter = null;
    private BluetoothGattCharacteristic mReadCharacter = null;
    public boolean isShutdownFinish = false;

//    private Handler mJobBatteryTestTimeoutHandler = new Handler();
//    private Runnable mJobBatteryTestRunnable = null;
    public String mac = null;

    private void stop(BluetoothGatt gatt, int code)
    {
      mCmdResultCode = code;

      if (gatt != null)
      {
        if (isShutdownFinish == false)
        {
          try
          {
            Log.d(TAG, String.format("BT Stop code:%d", code));
            gatt.disconnect();
          /*2017-07-09 在這裡 close 可能會造成 Excpetion.*/
            //gatt.close();
            //mGatt = null;
          /*2017-08-02 改為 重複使用 所以不要設為 null*/
            //myGatt = null;
          }
          catch (Exception e)
          {
            Log.d(TAG, String.format("stop Err:%s", e.toString()));
          }
        }

        mConnectionState = CONN_DISCONNECT;
      }
    }

    public void stopConnection()
    {
      if (myGatt != null && (isShutdownFinish == false))
      {
        try
        {
          /*理論上會觸發 onConnectionStateChange*/
          myGatt.disconnect();
          /*下一版加上 可能會比較好, 但後來改成 等待的機制, 所以還是不要加, 讓系統自動改變正確的狀態 */
//          mCmdResultCode = RESULT_FAIL;
//          mConnectionState = CONN_DISCONNECT;
        }catch(Exception e)
        {
          Log.d(TAG, String.format("stopConnection Err:%s", e.toString()));
        }
      }
    }

    public boolean isConnected()
    {
      return (mConnectionState == CONN_CONNECTED ||
              mConnectionState == CONN_CMD_TIMEOUT);
    }

//    public boolean doGhost()
//    {
//      boolean res = false;
//      if (mJobBatteryTestStep != 0 || !isConnected()  || myGatt == null)
//      {
//        return res;
//      }
//
//      mConnectionState = CONN_SEND_CMD;
//      /*送出命令...*/
//      BluetoothGattService serv = myGatt.getService(BT_BAT_AUTH_SERVICE_UUID.getUuid());
//      if (serv != null)
//      {
//        BluetoothGattCharacteristic cc = null;
//        cc = serv.getCharacteristic(BT_BAT_WRITE_CHARACTER_UUID.getUuid());
//        if (cc != null)
//        {
//          byte[] data = BatteryObj.getCmdSetTime();
//          mIsOnlySetTime = true;
//          data = BatteryObj.getCmdGhost();
//          //Log.d(TAG, "SET:" + LUtil.dumpBytes(data));
//          cc.setValue(data);
//          myGatt.writeCharacteristic(cc);
//        }
//      }
//
//      return true;
//    }


    public boolean DoBatteryTest(boolean isOnlySetTime)
    {
      boolean res = false;
      if (mJobBatteryTestStep != 0 || !isConnected()  || myGatt == null)
      {
        return res;
      }

//      mJobBatteryTestRunnable = new Runnable(){
//        @Override
//        public void run() {
//          Log.e(TAG, "Timeout Job Battery Test.");
//          mJobBatteryTestStep = 0;
//          /*其實 若成功 會移除 callback, 所以這一段程式碼應該不會執行到, 所以這邊不需要判斷*/
//          //if (mConnectionState == CONN_WAIT_REPORT || mConnectionState == CONN_SEND_CMD)
//          {
//            /*2017-07-11 將逾時的狀態設為  CONN_CMD_TIMEOUT*/
//            mConnectionState = CONN_CMD_TIMEOUT;
//            /*通知 UI 恢復, 顯示最後一筆*/
//            if (mBatteryHandler != null)
//            {
//              mBatteryHandler.notifyCommandTimeout();
//            }
//          }
//
//        }};
//
//      mJobBatteryTestTimeoutHandler.postDelayed(mJobBatteryTestRunnable, 6000);

      mConnectionState = CONN_SEND_CMD;
      /*送出命令...*/
//      BluetoothGattService serv = myGatt.getService(BT_BAT_AUTH_SERVICE_UUID.getUuid());
//      if (serv != null)
//      {
//        BluetoothGattCharacteristic cc = null;
//        cc = serv.getCharacteristic(BT_BAT_WRITE_CHARACTER_UUID.getUuid());
//        if (cc != null)
//        {
//          byte[] data = BatteryObj.getCmdSetTime();
//          mIsOnlySetTime = isOnlySetTime;
//          //Log.d(TAG, "SET:" + LUtil.dumpBytes(data));
//          cc.setValue(data);
//          myGatt.writeCharacteristic(cc);
//        }
//      }
      if (mWriteCharacter != null)
      {
        byte[] data = VehicleObj.getCmdSetTime();
        mIsOnlySetTime = isOnlySetTime;
        //Log.d(TAG, "SET:" + LUtil.dumpBytes(data));
        mWriteCharacter.setValue(data);
        myGatt.writeCharacteristic(mWriteCharacter);
      }

      return true;
    }


    // 偵測GATT client連線或斷線
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState)
    {
      Log.d(TAG, String.format("onConnectionStateChange s:%d, newS:%d", status, newState));
      if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED)
      {
        myGatt = gatt;
        Log.d(TAG, "Connected to GATT Server");
        gatt.discoverServices();
        isShutdownFinish = false;
      }
      else
      {
        if (status == BluetoothGatt.STATE_DISCONNECTED)
        {
          Log.d(TAG, "GATT Close");
          gatt.close();
          isShutdownFinish = true;
        }
        else
        {
          Log.d(TAG, "onConnectionStateChange but not Close.");
        }

        Log.d(TAG, String.format("Disconnected from GATT Server, mCmdResultCode:%d", mCmdResultCode));
        /*表示 一開始連線就失敗*/
        if (mCmdResultCode == RESULT_UNKNOW)
        {
          stop(gatt, RESULT_FAIL);
          //mCmdResultCode = RESULT_FAIL;
          //mConnectionState = CONN_DISCONNECT;
        }
        else
        {
          stop(gatt, RESULT_FAIL_AND_STOP);
        }
      }
    }

    //發現新的服務
    public void onServicesDiscovered(BluetoothGatt gatt, int status)
    {
      Log.d(TAG, "Discover & Config GATT Services");
      boolean isOK = false;
      BluetoothGattService serv = gatt.getService(BT_VEHICLE_AUTH_SERVICE_UUID.getUuid());
      if (serv != null)
      {
        BluetoothGattCharacteristic cc = null;
        mReadCharacter = serv.getCharacteristic(BT_VEHICLE_READ_CHARACTER_UUID.getUuid());
        /*記錄 write 之後就不用再尋找*/
        mWriteCharacter = serv.getCharacteristic(BT_VEHICLE_WRITE_CHARACTER_UUID.getUuid());
        if (mReadCharacter != null && mWriteCharacter != null)
        {
          mConnectionState = CONN_CONNECTED;
          mBTVehicleHandler.notifyConnected();
          isOK = true;
        }

        /*下面是設定 啟動 Notification */
        if (mReadCharacter != null)
        {
          gatt.setCharacteristicNotification(mReadCharacter, true);
          BluetoothGattDescriptor descriptor = mReadCharacter.getDescriptor(CCCD);
          descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
          gatt.writeDescriptor(descriptor);
        }
        else
        {
          stop(gatt, RESULT_FAIL_AND_STOP);
        }
      }
      else
      {
        stop(gatt, RESULT_FAIL_AND_STOP);
      }
      if (!isOK && mConnectingBatteryObj != null)
      {
        if (mConnectingBatteryObj.authState == VehicleObj.AUTH_CHECKING)
        {
          mConnectingBatteryObj.authState = VehicleObj.AUTH_FAIL;
          /*加入 存在清單中, 下次就不會再測試 */
          mDataObj.addVehicle(mConnectingBatteryObj);
        }
        else
        {
          VehicleObj origBat = mDataObj.getVehicleByMacInExistList(mConnectingBatteryObj.mac);
          if (origBat != null)
          {
            origBat.authState = VehicleObj.AUTH_FAIL;
          }

        }
      }
      //ssstep = 0;
      //SetupSensorStep(gatt);
    }

    public void onCharacteristicRead(BluetoothGatt gatt,
                                     BluetoothGattCharacteristic cc, int status)
    {
      Log.d(TAG, String.format("Read Value is:%s",LUtil.dumpBytes(cc.getValue())));
    }

    //特徵寫入結果
    public void onCharacteristicWrite(BluetoothGatt gatt,
                                      BluetoothGattCharacteristic characteristic, int status) {
      Log.d(TAG, "onCharacteristicWrite");
      //SetupSensorStep(gatt);
//      if (status == BluetoothGatt.GATT_SUCCESS)
//      {
//        //Log.d(TAG, String.format("WW Value is:%s", LUtil.dumpBytes(characteristic.getValue())));
//        if (mIsOnlySetTime)
//        {
//          /*只設定時間*/
//          mIsOnlySetTime = false;
//          mConnectionState = CONN_CONNECTED;
//        }
//        else
//        {
//          mJobBatteryTestStep++;
//          if (mJobBatteryTestStep == 2)
//          {
//            mConnectionState = CONN_WAIT_REPORT;
//            mJobBatteryTestStep = 0;
//          }
//          //Log.d("+Beacon", String.format("[W] -> GATT_SUCCESS"));
//
//          if (mJobBatteryTestStep == 1)
//          {
//
//            byte[] data = VehicleObj.getCmdTest();
//            //Log.d(TAG, "SET:" + LUtil.dumpBytes(data));
//            characteristic.setValue(data);
//            gatt.writeCharacteristic(characteristic);
//          }
//        }
//      }
//      else
//      {
//        Log.d("+Beacon", String.format("[W] -> GATT_FAIL"));
//        //broadcastUpdate(ACTION_DATA_WRITE_FAIL, characteristic);
//
//        stop(gatt, RESULT_FAIL_AND_STOP);
//
//      }
    }

    //描述寫入結果
    public void onDescriptorWrite(BluetoothGatt gatt,
                                  BluetoothGattDescriptor descriptor, int status)
    {
      Log.d(TAG, "onDescriptorWrite");

    }

    public void sendData(byte[] data)
    {
      if (myGatt != null && mWriteCharacter != null)
      {
        mWriteCharacter.setValue(data);
        myGatt.writeCharacteristic(mWriteCharacter);
      }
    }


    //遠端特徵通知結果
    byte[] mLastDecodeData = null;
    boolean mIsSendAck = false;
    public void onCharacteristicChanged(BluetoothGatt gatt,
                                        BluetoothGattCharacteristic characteristic) {
      byte[] val = characteristic.getValue();
      //byte[] codeVal = Arrays.copyOf(val, val.length);

      //Log.d(TAG, String.format("Change Value is:%s",LUtil.dumpBytes(val)));
      VehicleObj.decode(val);
      //Log.d(TAG, String.format("Dec:%s",LUtil.dumpBytes(val)));
      if (mLastDecodeData != null)
      {
        boolean isMatch = false;
        if (mLastDecodeData.length == val.length && mLastDecodeData.length > 1)
        {
          isMatch = true;
          for (int i = 1; i < mLastDecodeData.length - 1; i++)
          {
            if (mLastDecodeData[i] != val[i])
            {
              isMatch = false;
              break;
            }
          }
        }
        if (isMatch)
        {
          if (!mIsSendAck)
          {


            mIsSendAck = true;
          }
        }
        else
        {
          mIsSendAck = false;
        }

      }
      /*2017-08-02 改成每收到一次 就傳送一次 ACK*/
      if (mWriteCharacter != null)
      {
        mIsOnlySetTime = true;
        mWriteCharacter.setValue(VehicleObj.getCmdGhost());
        gatt.writeCharacteristic(mWriteCharacter);
      }

      mLastDecodeData = val;
      if (VehicleObj.isValidate(val))
      {
        //Log.d(TAG, String.format("Decode OK:%s",LUtil.dumpBytes(val)));
        VehicleObj vehicleObj = VehicleObj.fromPacket(val);
        if (vehicleObj != null)
        {
          if (val[1] == 0x02)
          {
            if (mConnectionState == CONN_WAIT_REPORT)
            {
//              if (mJobBatteryTestRunnable != null)
//              {
//                Log.d(TAG,"Got 0x02 Finish Job.");
//                mJobBatteryTestTimeoutHandler.removeCallbacks(mJobBatteryTestRunnable);
//              }
              mConnectionState = CONN_CONNECTED;
            }
          }
          /*儲存到資料庫, 不過要確認 時間是否正確?? */
          long tickNow = new Date().getTime();
          //Log.d(TAG, String.format("Record date:%s, subTick:%d",vehicleObj.recordTime.toString(),  (vehicleObj.recordTime.getTime() - tickNow)));
          /*檢查回報的時間不能是超過現在時間 100分鐘, */
          //if (vehicleObj.recordTime != null && ((vehicleObj.recordTime.getTime() - tickNow) < 6000000 ))
          if (vehicleObj.recordTime != null)
          {
            mDataObj.copyLinkInfo(vehicleObj);
            if (mBTVehicleHandler != null)
            {
              mBTVehicleHandler.saveVehicleToDB(vehicleObj);
            }
            //Log.d(TAG, String.format("Add Obj to DB:%s", vehicleObj.toString()));
          }
          else
          {
            Log.d(TAG, String.format("Obj time error!:%s", vehicleObj.toString()));
          }

        }
      }
      else
      {
        Log.d(TAG, String.format("Decode Failed!: %s",LUtil.dumpBytes(val)));
      }

    }
  };



}
